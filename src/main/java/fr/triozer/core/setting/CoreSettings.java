package fr.triozer.core.setting;

import fr.triozer.api.plugin.setting.Settings;
import fr.triozer.core.TrioCore;

/**
 * @author Cédric / Triozer
 */
public class CoreSettings extends Settings {

    private boolean autoUpdate;
    private boolean sql;
    private boolean stats;

    public CoreSettings() {
        super(TrioCore.getInstance(), "https://api.triozer.fr/plugins/2");

        this.configuration = this.plugin.getConfiguration().get("config");

        this.language = TrioCore.getInstance().getLanguage().get(this.configuration.getString("language"));

        this.autoUpdate = this.configuration.getBoolean("auto-update");
        this.sql = this.configuration.getBoolean("sql");
        this.stats = this.configuration.getBoolean("stats");

        if (isAutoUpdate()) this.check();
    }

    public final boolean isAutoUpdate() {
        return this.autoUpdate;
    }

    public final boolean isSQL() {
        return this.sql;
    }

    public final boolean isStats() {
        return this.stats;
    }

}
