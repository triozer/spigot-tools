package fr.triozer.core;

import fr.triozer.api.database.mysql.MySQL;
import fr.triozer.api.database.mysql.Table;
import fr.triozer.api.entity.Entities;
import fr.triozer.api.entity.TrioEntity;
import fr.triozer.api.game.Game;
import fr.triozer.api.i18n.I18N;
import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.api.rank.Ranks;
import fr.triozer.api.ui.InventoryBuilder;
import fr.triozer.core.commands.CoreCommand;
import fr.triozer.core.commands.LangCommand;
import fr.triozer.core.commands.MessageCommand;
import fr.triozer.core.commands.RankCommand;
import fr.triozer.core.configuration.CoreConfiguration;
import fr.triozer.core.rank.CoreRank;
import fr.triozer.core.setting.CoreSettings;
import fr.triozer.core.user.Users;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Cédric / Triozer
 */
public class TrioCore extends TrioPlugin {

	public static Table USERS;

	private static TrioCore instance;

	private List<InventoryBuilder> inventories;
	private List<TrioPlugin>       plugins;
	private List<Game>             games;

	private MySQL database;
	private Users players;

	private   Entities.Manager        entities;
	protected Ranks.Manager<CoreRank> ranks;

	public static TrioCore getInstance() {
		return instance;
	}

	@Override
	protected void load() {

	}

	@Override
	protected void registerCommands() {
		new CoreCommand().register();
		new MessageCommand().register();
		new LangCommand().register();
		new RankCommand().register();
	}

	@Override
	protected void registerListeners() {

	}

	@Override
	protected void initSQL() {
		USERS = TrioCore.getInstance().getTrioDatabase()
				.create("users",
						"`id` int NOT NULL AUTO_INCREMENT", "`uuid` VARCHAR(50) NOT NULL",
						"PRIMARY KEY (`id`)", "UNIQUE INDEX `uuid` (`uuid`)");
	}

	@Override
	protected void enable() {
		instance = this;

		this.configuration = new CoreConfiguration();
		this.settings = new CoreSettings();

		if (((CoreSettings) this.settings).isSQL()) {
			this.database = new MySQL(this.console,
					getConfig().getString("sql.host"),
					getConfig().getInt("sql.port"),
					getConfig().getString("sql.database"))
					.auth(getConfig().getString("sql.user"), getConfig().getString("sql.password"));

			initSQL();
		}

		this.ranks = new Ranks.Manager<>(this, CoreRank.class);
		this.entities = new Entities.Manager();
		this.inventories = new ArrayList<>();
		this.plugins = new ArrayList<>();
		this.games = new ArrayList<>();
		this.players = new Users();

		Bukkit.getOnlinePlayers().forEach(user -> this.players.add(user.getUniqueId()));
	}

	@Override
	protected void disable() {
		// this.players.values().filter(user -> user.getScoreboard() != null).forEach(user -> user.getScoreboard().end());
		// this.players.values().forEach(user -> user.getProperty().save());
		this.entities.values().forEach(TrioEntity::end);
		Bukkit.getOnlinePlayers().forEach(Player::closeInventory);
	}

	public String translate(String path, String... replace) {
		return I18N.Manager.translate(getSettings().getLanguage(), path, replace);
	}

	public void add(TrioPlugin plugin) {
		this.plugins.add(plugin);
	}

	public void add(Game game) {
		this.games.add(game);
	}

	public void add(InventoryBuilder inventoryBuilder) {
		this.inventories.add(inventoryBuilder);
	}

	public boolean is(String name) {
		return plugins().anyMatch(plugin -> plugin.getName().equals(name));
	}

	public TrioPlugin get(String name) {
		return plugins().filter(plugin -> plugin.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
	}

	public final Stream<Game> games() {
		return this.games.stream();
	}

	public final Stream<TrioPlugin> plugins() {
		return this.plugins.stream();
	}

	public final Stream<InventoryBuilder> getInventories() {
		return this.inventories.stream();
	}

	public final List<InventoryBuilder> getInventoriesList() {
		return this.inventories;
	}

	public final MySQL getTrioDatabase() {
		return this.database;
	}

	public final CoreSettings getSettings() {
		return (CoreSettings) this.settings;
	}

	public final Users getPlayers() {
		return this.players;
	}

	public final Entities.Manager getEntities() {
		return this.entities;
	}

	public final Ranks.Manager<CoreRank> getRanks() {
		return this.ranks;
	}

}
