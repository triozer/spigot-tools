package fr.triozer.core.rank;

import fr.triozer.api.rank.Rank;
import fr.triozer.core.TrioCore;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.scoreboard.Team;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Cédric / Triozer
 */
public class CoreRank implements Rank {

    private int    id;
    private String name;

    private ChatColor tag;

    private Set<String> permissionsList;

    public CoreRank(int id, String name, ChatColor tag, List<String> permissions) {
        this.id = id;
        this.name = name;
        this.tag = tag;
        this.permissionsList = new HashSet<>();

        this.permissionsList.addAll(permissions);
    }

    public CoreRank(int id, String name, ChatColor tag, String... permissions) {
        this(id, name, tag, Arrays.asList(permissions));
    }

    @Override
    public void addPermission(Set<String> permissions) {
        this.permissionsList.addAll(permissions);
    }

    @Override
    public void addPermission(String... permissions) {
        this.permissionsList.addAll(Arrays.asList(permissions));
    }

    @Override
    public void removePermission(String... permissions) {
        this.permissionsList.removeAll(Arrays.asList(permissions));
    }

    @Override
    public final Set<String> getPermissionsList() {
        return this.permissionsList;
    }

    @Override
    public final int getID() {
        return this.id;
    }

    @Override
    public final String getName() {
        return this.name;
    }

    @Override
    public final String getPrefix() {
        return this.tag + "[" + getName() + "]";
    }

    @Override
    public final ChatColor getTag() {
        return this.tag;
    }

    @Override
    public final Team getTeam() {
        return TrioCore.getInstance().getRanks().getTeams().get(this);
    }

    @Override
    public boolean showPrefix() {
        return this.id != 0;
    }

}
