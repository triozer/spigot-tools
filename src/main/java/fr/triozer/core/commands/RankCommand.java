package fr.triozer.core.commands;

import fr.triozer.api.command.AbstractCommand;
import fr.triozer.api.message.ChatMessage;
import fr.triozer.api.message.Message;
import fr.triozer.api.user.User;
import fr.triozer.core.TrioCore;
import fr.triozer.core.rank.CoreRank;
import org.apache.commons.lang3.math.NumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Cédric / Triozer
 */
public class RankCommand extends AbstractCommand {

    public RankCommand() {
        super(TrioCore.getInstance(), "rank", "rank", false, 0);
    }

    @Override
    public void execute(Player player, String[] args) {
        TrioCore core = ((TrioCore) plugin);
        User     user = core.getPlayers().getUser(player);
        if (args.length == 0) {
            user.sendTranslatedMessage("command.rank.current", user.getRank().getPrefix());
        }
        if ("set".equalsIgnoreCase(args[0]) && user.hasPermission(this.permission + ".set")) {
            if (args.length < 3) {
                help(player, "/rank set <id>");
                return;
            }

            if (Bukkit.getPlayerExact(args[1]) == null) {
                help(player, "Please set a valid player.");
                return;
            }

            User target = core.getPlayers().getUser(Bukkit.getPlayerExact(args[1]));

            CoreRank rank;
            if (NumberUtils.isParsable(args[2]))
                rank = core.getRanks().get(Integer.parseInt(args[2]));
            else {
                help(player, "  §cPlease set an valid ID.");
                return;
            }

            core.getRanks().setRank(target, rank);
        } else if ("list".equalsIgnoreCase(args[0]) && user.hasPermission(this.permission + ".list")) {
            if (args.length < 2) {
                player.sendMessage("  §cPas assez de paramètres");
                return;
            }

            CoreRank rank;
            if (NumberUtils.isParsable(args[1]))
                rank = core.getRanks().get(Integer.parseInt(args[1]));
            else {
                help(player, "  §cPlease set an valid ID.");
                return;
            }

            player.sendMessage("Permissions du grade : " + rank.getName());
            for (String permission : rank.getPermissionsList()) {
                player.sendMessage("  - §b" + permission.replace("$", ""));
            }
            player.sendMessage("");

        }

    }

    @Override
    protected List<String> tabCompleter(Player player, String[] strings) {
        return null;
    }

    @Override
    protected List<Message> addHelp(CommandSender commandSender) {
        User         user = TrioCore.getInstance().getPlayers().getUser((Player) commandSender);
        List<Message> help = new ArrayList<>();

        help.add(new ChatMessage(buildSubcommand("rank", user.getLocale(TrioCore.getInstance().getLanguage()).translate("command.rank.show"))));
        if (user.hasPermission(this.permission + ".set"))
            help.add(new ChatMessage(buildSubcommand("rank", "Show your current rank.")));
        if (user.hasPermission(this.permission + ".list"))
            help.add(new ChatMessage(buildSubcommand("rank", "Show your current rank.")));
        return help;
    }

}
