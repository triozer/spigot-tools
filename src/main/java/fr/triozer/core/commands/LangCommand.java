package fr.triozer.core.commands;

import fr.triozer.api.command.AbstractCommand;
import fr.triozer.api.i18n.I18N;
import fr.triozer.api.message.ChatMessage;
import fr.triozer.api.message.ClickableChatMessage;
import fr.triozer.api.message.Message;
import fr.triozer.api.user.User;
import fr.triozer.api.util.TextBuilder;
import fr.triozer.core.TrioCore;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static net.md_5.bungee.api.ChatColor.DARK_GRAY;

/**
 * @author Cédric / Triozer
 */
public class LangCommand extends AbstractCommand {

    private final TrioCore core = (TrioCore) plugin;

    public LangCommand() {
        super(TrioCore.getInstance(), "lang", null, false, 1);
    }

    @Override
    protected void execute(Player sender, String[] args) {
        if (args.length == 0) {
            showHelp(sender);
            return;
        }

        User user = core.getPlayers().getUser(sender);
        if ("list".equalsIgnoreCase(args[0])) {
            List<Message> messages = new ArrayList<>();

            messages.add(new ChatMessage(DARK_GRAY + " *--------------------------------------------------*"));
            messages.add(new ChatMessage(""));
            messages.add(new ChatMessage("  " + user.getLocale(plugin.getLanguage()).translate("command.lang.description") + " §8(" + core.getLanguage().values().count() + ")"));
            core.getLanguage().values().forEach(lang ->
                    messages.add(new ClickableChatMessage(new TextBuilder("    " + lang.getLang())
                            .hove(HoverEvent.Action.SHOW_TEXT, lang.translate("command.lang.choose"))
                            .click(ClickEvent.Action.RUN_COMMAND, "/lang set " + lang.getCode())
                            .build())));
            messages.add(new ChatMessage(""));
            messages.add(new ChatMessage(DARK_GRAY + " *--------------------------------------------------*"));

            messages.forEach(message -> message.send(sender));

        } else if ("set".equalsIgnoreCase(args[0])) {
            I18N locale = TrioCore.getInstance().getLanguage().get(args[1]);
            if (locale == null) {
                user.sendTranslatedMessage("command.lang.error", locale.getLang());
                return;
            }
            user.setLocale(locale);
            user.sendTranslatedMessage("command.lang.success", user.getLocale(TrioCore.getInstance().getLanguage()).getLang(), locale.getLang());
            // displayHelp("set", sender);
        }
    }

    @Override
    public List<Message> addHelp(CommandSender sender) {
        List<Message> messages = new ArrayList<>();
        User         user     = TrioCore.getInstance().getPlayers().getUser((Player) sender);
        I18N         locale   = user.getLocale(plugin.getLanguage());

        messages.add(new ChatMessage(buildSubcommand("list", locale.translate("command.lang.list"))));
        messages.add(new ChatMessage(buildSubcommand("set", locale.translate("command.lang.set"), "lang")));

        return messages;
    }

    @Override
    protected void execute(CommandSender sender, String[] args) {
        if ("list".equalsIgnoreCase(args[0])) {

            List<Message> messages = new ArrayList<>();

            messages.add(new ChatMessage("§8[§eTrioCore§8] §bList of avaiable languages §8(" + core.getLanguage().values().count() + ")"));
            messages.add(new ChatMessage(""));
            core.getLanguage().values().forEach(lang -> messages.add(new ChatMessage("- " + lang.getLang())));
            messages.add(new ChatMessage(""));

            messages.forEach(message -> message.send(sender));

        } else {
            showHelp(sender);
        }

    }

    @Override
    protected List<String> tabCompleter(CommandSender sender, String[] args) {
        return super.tabCompleter(sender, args);
    }

}
