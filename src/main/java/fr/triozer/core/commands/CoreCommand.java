package fr.triozer.core.commands;

import fr.triozer.api.command.AbstractCommand;
import fr.triozer.api.i18n.I18N;
import fr.triozer.api.message.ChatMessage;
import fr.triozer.api.message.Message;
import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.api.user.User;
import fr.triozer.core.TrioCore;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import java.text.SimpleDateFormat;
import java.util.*;

import static net.md_5.bungee.api.ChatColor.DARK_GRAY;

/**
 * @author Cédric / Triozer
 */
public class CoreCommand extends AbstractCommand {
    public CoreCommand() {
        super(TrioCore.getInstance(), "core", "core", true, 1);
    }

    @Override
    protected void execute(Player sender, String[] args) {
        TrioCore core = (TrioCore) plugin;
        User     user = core.getPlayers().getUser(sender);

        if ("list".equalsIgnoreCase(args[0])) {
            String id = UUID.randomUUID().toString().split("-")[0];

            List<ChatMessage> messages = new ArrayList<>();

            messages.add(new ChatMessage(DARK_GRAY + " *--------------------------------------------------*"));
            messages.add(new ChatMessage(""));
            messages.add(new ChatMessage("  §7" + id + " §b - §b" + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date())));
            messages.add(new ChatMessage(""));
            messages.add(new ChatMessage("  §7" + id + " §b - §7Plugins §8(" + core.plugins().count() + ")"));
            core.plugins().forEach(plugin -> {
                PluginDescriptionFile description = plugin.getDescription();
                messages.add(new ChatMessage("  §b— §6" + description.getName() + " §8v" + description.getVersion()));
            });
            messages.add(new ChatMessage(""));
            messages.add(new ChatMessage(DARK_GRAY + " *--------------------------------------------------*"));

            messages.forEach(message -> message.send(sender));
        } else if ("reload".equalsIgnoreCase(args[0]) && user.getRank().hasPermission(this.permission + ".reload")) {

            if (args.length == 1) {
                new ChatMessage("  §aReloading §7`" + core.getName() + "`§a.").send(sender);
                core.onDisable();
                core.onEnable();
                new ChatMessage("  §aReloaded §7`" + core.getName() + "`§a.").send(sender);
                return;
            }

            if (core.get(args[1]) == null) {
                new ChatMessage("  §7`" + args[1] + "` §cis not a plugin supported by TrioCore.").send(sender);
            } else {
                TrioPlugin plugin = core.get(args[1]);

                new ChatMessage("  §aReloading §7`" + args[1] + "`§a.").send(sender);
                plugin.onDisable();
                plugin.onEnable();
                new ChatMessage("  §aReloaded §7`" + args[1] + "`§a.").send(sender);
            }

        } else if ("entities".equalsIgnoreCase(args[0])) {
            long count = core.getEntities().values().count();
            new ChatMessage(core.getPlayers().getUser(sender).getLocale(TrioCore.getInstance().getLanguage()),
                    "command.triocore.entities", Long.toString(count), count > 1 ? "s" : "")
                    .send(sender);
        }
    }

    @Override
    public List<Message> addHelp(CommandSender sender) {
        List<Message> messages = new ArrayList<>();
        User         user     = TrioCore.getInstance().getPlayers().getUser((Player) sender);
        I18N         locale   = user.getLocale(plugin.getLanguage());

        messages.add(new ChatMessage(buildSubcommand("entities", locale.translate("command.core.entities"))));
        messages.add(new ChatMessage(buildSubcommand("list", locale.translate("command.core.list"))));
        if (user.hasPermission(this.permission + ".reload"))
            messages.add(new ChatMessage(buildSubcommand("reload", "Reload a plugin.", "plugin")));

        return messages;
    }

    @Override
    protected List<String> tabCompleter(Player player, String[] args) {
        return new ArrayList<>();
    }

}
