package fr.triozer.core.commands;

import fr.triozer.api.command.AbstractCommand;
import fr.triozer.api.message.*;
import fr.triozer.api.user.User;
import fr.triozer.api.util.TextBuilder;
import fr.triozer.core.TrioCore;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * @author Cédric / Triozer
 */
public class MessageCommand extends AbstractCommand {
    public MessageCommand() {
        super(TrioCore.getInstance(), "message", "message", true, 0);
    }

    @Override
    protected void execute(Player player, String[] args) {

        Message       message;
        StringBuilder text = new StringBuilder(args[1]);
        for (int i = 2; i < args.length; i++) {
            text.append(" ").append(args[i]);
        }
        String s = ChatColor.translateAlternateColorCodes('&', text.toString());

        if ("actionbar".equalsIgnoreCase(args[0])) {
            message = new ActionbarMessage(s);
        } else if ("title".equalsIgnoreCase(args[0])) {
            message = new TitleMessage(s.split("~")[0], s.split("~")[1], 10);
        } else if ("json".equalsIgnoreCase(args[0])) {
            message = new ClickableChatMessage(new TextBuilder(s));
        } else {
            message = new ChatMessage(s);
        }

        message.send(player);
    }


    @Override
    public List<Message> addHelp(CommandSender sender) {
        List<Message> messages = new ArrayList<>();
        User         user     = TrioCore.getInstance().getPlayers().getUser((Player) sender);

        messages.add(new ChatMessage(buildSubcommand("start", "Start a game.", "game")));

        return messages;
    }

    @Override
    protected List<String> tabCompleter(Player player, String[] args) {
        return Collections.emptyList();
    }
}
