package fr.triozer.core.configuration;

import fr.triozer.api.plugin.configuration.BaseConfiguration;
import fr.triozer.core.TrioCore;

/**
 * @author Cédric / Triozer
 */
public class CoreConfiguration extends BaseConfiguration {

	public CoreConfiguration() {
		super(TrioCore.getInstance());
	}

}
