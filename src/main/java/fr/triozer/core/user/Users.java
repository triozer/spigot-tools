package fr.triozer.core.user;

import fr.triozer.api.property.DataProperty;
import fr.triozer.api.user.User;
import fr.triozer.core.TrioCore;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * @author Cédric / Triozer
 */
public class Users {

    private Map<UUID, User> uuidToUser;

    public Users() {
        this.uuidToUser = new HashMap<>();

        Bukkit.getPluginManager().registerEvents(new Listeners(), TrioCore.getInstance());
    }

    public final User add(UUID uuid) {
        if (!this.uuidToUser.containsKey(uuid)) {
            User user = new User(uuid, new DataProperty(uuid, TrioCore.getInstance().getConfiguration()
                    .create("datas/" + uuid.toString())));
            this.uuidToUser.put(uuid, user);
        }

        return this.uuidToUser.get(uuid);
    }

    public final User getUser(UUID uuid) {
        return this.uuidToUser.get(uuid);
    }

    public final User getUser(Player player) {
        return getUser(player.getUniqueId());
    }

    public final void remove(Player player) {
        this.uuidToUser.remove(player.getUniqueId());
    }

    public final Stream<User> values() {
        return this.uuidToUser.values().stream();
    }

    private final class Listeners implements Listener {
        @EventHandler(priority = EventPriority.HIGHEST)
        public void onPlayerLogin(PlayerLoginEvent event) {
            add(event.getPlayer().getUniqueId());
        }

        @EventHandler
        public void onPlayerJoin(PlayerJoinEvent event) {
            event.getPlayer().setScoreboard(TrioCore.getInstance().getRanks().getScoreboard());
        }

        @EventHandler
        public void onPlayerQuit(PlayerQuitEvent event) {
            getUser(event.getPlayer()).getProperty().save();
            remove(event.getPlayer());
        }
    }

}
