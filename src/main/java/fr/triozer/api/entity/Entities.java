package fr.triozer.api.entity;

import fr.triozer.api.manager.AbstractManager;
import fr.triozer.core.TrioCore;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Cette classe gère les entités {@link fr.triozer.api.entity.TrioEntity} créées par les plugins.
 *
 * @author Cédric / Triozer
 */
public class Entities {

    /**
     * Cette classe s'occupe de mettre à jour les entités.
     */
    private static class Task extends BukkitRunnable {
        @Override
        public void run() {
            Manager manager = TrioCore.getInstance().getEntities();
            if (manager.isUpdate()) {
                manager.values().forEach(TrioEntity::update);
            }
        }
    }

    /**
     * Cette classe gère les entités en elles-mêmes. Ajout, suppression, listage.
     */
    public static class Manager implements AbstractManager<TrioEntity, UUID> {

        private final Map<UUID, TrioEntity> entities;
        private       boolean               update;

        public Manager() {
            this.entities = new HashMap<>();
            this.update = true;
            new Task().runTaskTimerAsynchronously(TrioCore.getInstance(), 0L, 20L);
        }

        public final boolean isUpdate() {
            return this.update;
        }

        public void setUpdate(boolean update) {
            this.update = update;
        }

        @Override
        public TrioEntity get(UUID key) {
            return this.entities.get(key);
        }

        @Override
        public void add(TrioEntity value) {
            this.entities.putIfAbsent(value.getID(), value);
        }

        @Override
        public void remove(TrioEntity value) {
            value.end();
            this.entities.remove(value.getID());
        }

        @Override
        public Stream<TrioEntity> values() {
            return this.entities.values().stream();
        }

    }

}
