package fr.triozer.api.entity;

import fr.triozer.core.TrioCore;
import org.bukkit.entity.EntityType;

import java.util.UUID;

/**
 * @author Cédric / Triozer
 */
public abstract class TrioEntity<T> {

    private final UUID       id;
    private final EntityType type;

    protected T entity;

    public TrioEntity(EntityType type) {
        this.id = UUID.randomUUID();
        this.type = type;

        TrioCore.getInstance().getEntities().add(this);
    }

    public abstract void end();

    public abstract void update();

    public final UUID getID() {
        return this.id;
    }

    public final T getEntity() {
        return this.entity;
    }

    public final EntityType getType() {
        return this.type;
    }

}
