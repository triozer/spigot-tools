package fr.triozer.api.plugin;

import fr.triozer.api.game.manager.GameManager;
import fr.triozer.api.i18n.I18N;
import fr.triozer.api.metric.MetricsLite;
import fr.triozer.api.plugin.configuration.Configuration;
import fr.triozer.api.plugin.setting.Settings;
import fr.triozer.api.util.Console;
import fr.triozer.core.TrioCore;
import fr.triozer.core.setting.CoreSettings;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Cette classe est la classe principale de tous les plugins.
 * <p>
 * Elle s'occupe de gérer une grande partie de leur fonctionnement.
 *
 * @author Cédric / Triozer.
 */
public abstract class TrioPlugin extends JavaPlugin {

    protected Console console;

    protected Configuration configuration;
    protected GameManager   gameManager;
    protected Settings      settings;

    protected I18N.Manager  language;

    private boolean error;

    /**
     * Custom load
     */
    protected void load() {
    }

    /**
     * Custom enable
     */
    protected abstract void enable();

    /**
     * Custom disable
     */
    protected abstract void disable();

    /**
     * Register commands
     */
    protected void registerCommands() {
    }

    /**
     * Register listeners
     */
    protected void registerListeners() {
    }

    protected void initSQL() {
    }

    @Override
    public void onLoad() {
        load();
    }

    @Override
    public void onDisable() {
        // si il y a une erreur lors de la désactivation du plugin
        if (this.error) return;

        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().cancelTasks(this);

        disable();
    }

    @Override
    public void onEnable() {
        this.console = new Console(getName());

        // en sachant que tous les plugins dépendent de TrioCore, ce dernier sera forcément chargé avant les autres
        if (!isCore()) {
            TrioCore.getInstance().add(this);

            this.console.fine("Successfully registered with TrioCore");
            this.console.fine(getDescription().getName() + " v" + getDescription().getVersion() + " has been enabled ! If you appreciate this plugin, you can support the author by rating the plugin at www.spigotmc.org !");
        }

        this.language = new I18N.Manager();
        enable();

        registerCommands();
        registerListeners();

        if (isCore() && ((CoreSettings) this.settings).isStats()) new MetricsLite(this);
    }

    /**
     * Désactiver le plugin pour une quelconque raison.
     *
     * @param why la raison.
     */
    public void disable(String why) {
        this.error = true;
        this.console.danger("Disabling because : " + why);
        Bukkit.getPluginManager().disablePlugin(this);
    }

    @Override
    public String toString() {
        return getName();
    }

    public boolean isCore() {
        return getName().equals("TrioCore");
    }

    public final Console getConsole() {
        return this.console;
    }

    public final Configuration getConfiguration() {
        return this.configuration;
    }

    public final GameManager getGameManager() {
        return this.gameManager;
    }

    public final I18N.Manager getLanguage() {
        return this.language;
    }

}