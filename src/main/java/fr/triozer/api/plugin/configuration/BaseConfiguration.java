package fr.triozer.api.plugin.configuration;

import fr.triozer.api.i18n.I18N;
import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.core.TrioCore;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Cédric / Triozer
 */
public class BaseConfiguration implements Configuration {

	protected final TrioPlugin                     plugin;
	protected final Map<String, YamlConfiguration> configs;
	protected final Map<String, File>              configFiles;
	protected final File                           folder;

	public BaseConfiguration(TrioPlugin plugin) {
		this.plugin = plugin;
		this.configs = new HashMap<>();
		this.configFiles = new HashMap<>();
		this.folder = plugin.getDataFolder();

		init();
	}

	@Override
	public void init() {
		if (this.folder.exists() && this.folder.list() != null) {
			File _file = new File(this.folder, "config.yml");
			if (!_file.exists()) this.plugin.saveResource("config.yml", false);

			_file = new File(this.folder, "messages/");
			if (!_file.exists()) {
				this.saveMessages();
			}

			Set<File> files = new HashSet<>();

			try {
				Files.walk(this.folder.toPath())
						.filter(path -> Files.isRegularFile(path)
								&& (path.toFile().getName().endsWith(".yml") || path.toFile().getName().endsWith(".sign")))
						.forEach(file -> files.add(file.toFile()));
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (!files.isEmpty()) {
				files.forEach(file -> {
					String parent = this.folder.toPath().relativize(file.toPath().getParent()).toString();
					plugin.getConsole().fine("Loading " + parent + "/" + file.toPath().getFileName());

					this.load(file, "yml");

					if (parent.equalsIgnoreCase("messages")) {
						String            fileName = file.toPath().getFileName().toString().replace(".yml", "");
						YamlConfiguration load     = get(parent + "/" + fileName);

						plugin.getLanguage().add(new I18N(load.getString("name"), fileName, load));
					}
				});
			} else {
				createDefault();
			}
		} else {
			this.folder.mkdirs();
			createDefault();
		}
	}

	@Override
	public void saveMessages() {
		TrioCore.getInstance().saveResource("messages/fr_FR.yml", false);
		TrioCore.getInstance().saveResource("messages/en_US.yml", false);
	}

	@Override
	public void createDefault() {
		this.plugin.getConsole().warning("Saving default configurations.");
		this.plugin.saveResource("config.yml", false);
		this.saveMessages();
	}

	@Override
	public YamlConfiguration create(String name) {
		return create(name, "yml");
	}

	@Override
	public YamlConfiguration create(String name, String extension) {
		File file = new File(this.folder, name + "." + extension);

		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				this.plugin.getConsole().stacktrace("Can't create " + name + " ." + extension, e);
			}
		}

		return load(file, extension);
	}

	@Override
	public YamlConfiguration load(File file, String extension) {
		YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

		String parent = file.toPath().getParent().getFileName().toString();

		if (parent.equalsIgnoreCase(this.plugin.getName()))
			parent = "";
		else
			parent += "/";

		String name = parent + file.toPath().getFileName().toString().replace("." + extension, "");

		this.configs.put(name, configuration);
		this.configFiles.put(name, file);

		return configuration;
	}

	@Override
	public YamlConfiguration load(String name, String extension) {
		return load(new File(folder, name + "." + extension), extension);
	}

	@Override
	public void remove(String name) {
		this.remove(name, "yml");
	}

	@Override
	public void remove(String name, String extension) {
		if (has(name)) {
			name = name + "." + extension;
			File file = new File(this.folder, name);

			if (file.delete()) this.configs.remove(name);
			else this.plugin.getConsole().error("Can't remove " + name + "." + extension + "!");
		}
	}

	@Override
	public void save(String name) {
		try {
			this.configs.get(name).save(this.configFiles.get(name));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void save() {
		this.configs.keySet().forEach(this::save);
	}

	@Override
	public YamlConfiguration get(String path) {
		return this.configs.getOrDefault(path, null);
	}

	@Override
	public File getFile(String path) {
		return this.configFiles.getOrDefault(path, null);
	}

	@Override
	public boolean has(String name) {
		return this.configs.containsKey(name);
	}

	@Override
	public File getFolder() {
		return this.folder;
	}

	@Override
	public Stream<YamlConfiguration> getConfigurations() {
		return this.configs.values().stream();
	}

	@Override
	public Stream<Map.Entry<String, YamlConfiguration>> set() {
		return this.configs.entrySet().stream();
	}

}
