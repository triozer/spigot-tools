package fr.triozer.api.plugin.configuration;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Cette classe est le modèle de base pour la création de configuration YAML.
 *
 * @author Cédric / Triozer
 */
public interface Configuration {

    /**
     * Initialiser des configurations.
     */
    void init();

    /**
     * Créer les configurations par défaut utile aux plugins.
     */
    void saveMessages();

    /**
     * Créer les configurations par défaut utile aux plugins.
     */
    void createDefault();

    /**
     * Créer une nouvelle configuration YAML.
     *
     * @param name le nom de la configuration.
     * @return la configuration YAML.
     */
    YamlConfiguration create(String name);

    /**
     * Créer une nouvelle configuration avec une extension différente.
     *
     * @param name      le nom de la configuration.
     * @param extension l'extension de la configuration
     * @return la configuration au format YAML.
     */
    YamlConfiguration create(String name, String extension);

    /**
     * Charger une configurtion YAML à partir d'un fichier.
     *
     * @param file le fichier YAML.
     * @return la configuration YAML.
     */
    YamlConfiguration load(File file, String extension);

    /**
     * Charger une configurtion YAML à partir de son nom.
     *
     * @param name le nom du fichier YAML.
     * @return la configuration YAML.
     */
    YamlConfiguration load(String name, String extension);

    /**
     * Supprimer une configuration de ceux gérées par le plugin.
     *
     * @param name le nom de la configuration.
     */
    void remove(String name);


    /**
     * Supprimer une configuration de ceux gérées par le plugin.
     *
     * @param name le nom de la configuration.
     */
    void remove(String name, String extension);

    /**
     * Enregistrer une configuration.
     *
     * @param name le nom de la configuration.
     */
    void save(String name);

    /**
     * Enregistrer toutes les configurations.
     */
    void save();

    /**
     * Vérifie si la configuration est gérée par le plugin.
     *
     * @param name le nom de la configuration.
     * @return <code>true</code> si la configuration est gérée par le plugin.
     */
    boolean has(String name);

    /**
     * Récupère la configuration YAML à partir d'un nom.
     *
     * @param path le nom.
     * @return la configuration YAML.
     */
    YamlConfiguration get(String path);

    /**
     * Récupère le fichier de la configuration YAML.
     *
     * @param path le chemin du fichier.
     * @return le fichier.
     */
    File getFile(String path);

    /**
     * Retourne le dossier racine.
     *
     * @return le dossier racine.
     */
    File getFolder();

    /**
     * Retourne une liste des configurations gérées par le plugin.
     *
     * @return une liste des configurations gérées par le plugin.
     */
    Stream<YamlConfiguration> getConfigurations();

    Stream<Map.Entry<String, YamlConfiguration>> set();


}
