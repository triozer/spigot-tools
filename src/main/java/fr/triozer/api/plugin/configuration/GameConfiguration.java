package fr.triozer.api.plugin.configuration;

import fr.triozer.api.plugin.TrioPlugin;

/**
 * @author Cédric / Triozer
 */
public abstract class GameConfiguration extends BaseConfiguration {

    public GameConfiguration(TrioPlugin plugin) {
        super(plugin);
    }

    @Override
    public abstract void init();

    @Override
    public void createDefault() {
        super.createDefault();

        plugin.saveResource("config.yml", false);
        plugin.saveResource("messages/fr_FR.yml", false);
    }

}
