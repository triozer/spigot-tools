package fr.triozer.api.plugin.setting;

import com.google.gson.JsonParser;
import fr.triozer.api.i18n.I18N;
import fr.triozer.api.plugin.TrioPlugin;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Cette classe gère les paramètres contenus dans le <b>config.yml</b>.
 *
 * @author Cédric / Triozer
 */
public class Settings {

    protected final TrioPlugin        plugin;
    protected final String            update;
    protected       YamlConfiguration configuration;

    protected I18N language;

    public Settings(TrioPlugin plugin, String update) {
        this.plugin = plugin;
        this.update = update;
    }

    protected void check() {
        try {
            this.plugin.getConsole().fine("Searching for updates.");

            if (checkUpdate())
                this.plugin.getConsole().warning("Please update the plugin for a better support.");
            else
                this.plugin.getConsole().fine("No update found !");
        } catch (IOException e) {
            this.plugin.getConsole().stacktrace("Can't check for update", e);
        }
    }

    /**
     * Vérifier si une mise à jour est disponible.
     *
     * @return <code>true</code> si une mise à jour est disponible.
     * @throws IOException si il y a un problème de connexion.
     */
    public boolean checkUpdate() throws IOException {
        URL               url        = new URL(this.update);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");

        if (connection.getResponseCode() != 200) {
            //   plugin.getConsole().warning("Can't get last version from " + this.update);
            return false;
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String lastVersion = new JsonParser().parse(reader.readLine()).getAsJsonObject().get("version").getAsString();

        if (lastVersion.isEmpty()) return false;
        else if (!lastVersion.matches("(?!\\.)(\\d+(\\.\\d+)+)(?:[-.]+)?(?![\\d.])$")) return false;

        return !plugin.getDescription().getVersion().equals(lastVersion);
    }

    public final TrioPlugin getPlugin() {
        return this.plugin;
    }

    public final String getUpdate() {
        return this.update;
    }

    public final YamlConfiguration getConfiguration() {
        return this.configuration;
    }

    public final I18N getLanguage() {
        return this.language;
    }

    /**
     * Changer la langue générale du plugin.
     *
     * @param language la nouvelle langue.
     */
    public void setLanguage(I18N language) {
        this.language = language;
    }

}
