package fr.triozer.api.command;

import fr.triozer.api.message.ChatMessage;
import fr.triozer.api.message.Message;
import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.api.util.DefaultFontInfo;
import fr.triozer.core.TrioCore;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.*;

import static net.md_5.bungee.api.ChatColor.*;

/**
 * Cette classe sert à simplifier l'implémentation de commandes sur un serveur minecraft.
 *
 * @author Cédric / Triozer
 */
public abstract class AbstractCommand implements CommandExecutor, TabCompleter {

    protected TrioPlugin plugin;

    protected String  commandName;
    protected String  permission;
    private   boolean onlyPlayersCommand;
    private   int     minArgs;

    /**
     * Le constructeur principal de classe.
     *
     * @param plugin             le plugin lié à la commande.
     * @param commandName        la commande.
     * @param permission         la permission nécessaire pour éxecuter la commande, peut être nulle.
     * @param onlyPlayersCommand précise si la commande peut-être utilisée par la console.
     * @param minArgs            le nombre minimum d'argument pour que la commande soit éxecutée.
     */
    public AbstractCommand(TrioPlugin plugin, String commandName, @Nullable String permission, boolean onlyPlayersCommand, int minArgs) {
        this.plugin = plugin;
        this.commandName = commandName;
        this.permission = permission;
        this.onlyPlayersCommand = onlyPlayersCommand;
        this.minArgs = minArgs;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        boolean isPlayer = sender instanceof Player;

        if (onlyPlayersCommand && !isPlayer) {
            plugin.getConsole().error(TrioCore.getInstance().translate("command.only-player"));
            return true;
        }

        if (permission != null && !TrioCore.getInstance().getPlayers().getUser((Player) sender).hasPermission(permission)) {
            return true;
        }

        if (minArgs >= 0 && args.length < minArgs) {
            showHelp(sender);

            return true;
        }

        if ("help".equalsIgnoreCase(args[0]) || "?".equalsIgnoreCase(args[0]) || "h".equalsIgnoreCase(args[0])) {
            showHelp(sender);
            return true;
        }

        if (isPlayer)
            execute((Player) sender, args);
        else
            execute(sender, args);

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        boolean isPlayer = sender instanceof Player;

        if (onlyPlayersCommand && !isPlayer) {
            plugin.getConsole().error(TrioCore.getInstance().translate("command.only-player"));
            return Collections.emptyList();
        }

        if (label.equalsIgnoreCase(commandName)) {
            if (permission != null)
                if (!sender.hasPermission(permission)) {
                    return Collections.emptyList();
                }

            return onlyPlayersCommand && isPlayer ? tabCompleter((Player) sender, args) : tabCompleter(sender, args);
        }

        return Collections.emptyList();
    }

    /**
     * Cette méthode enregistre la commande et sa complétion à l'aide de la tabulation.
     */
    public void register() {
        plugin.getCommand(commandName).setExecutor(this);
        plugin.getCommand(commandName).setTabCompleter(this);
    }

    /**
     * Cette méthode est l'implémentation de la commande pour une console.
     *
     * @param console l'objet qui execute la commande.
     * @param args    les arguments utilisées pour la commande.
     */
    protected void execute(CommandSender console, String[] args) {
    }

    /**
     * Cette méthode est l'implémentation de la commande pour un joueur.
     *
     * @param player le joueur qui execute la commande.
     * @param args   les arguments utilisées pour la commande.
     */
    protected void execute(Player player, String[] args) {
    }

    /**
     * Cette méthode est l'implémentation de la complétition à l'aide de la tabulation pour une console.
     *
     * @param console l'objet qui execute la commande.
     * @param args    les arguments utilisées pour la commande.
     */
    protected List<String> tabCompleter(CommandSender console, String[] args) {
        return Collections.emptyList();
    }

    /**
     * Cette méthode est l'implémentation de la complétition à l'aide de la tabulation pour un joueur.
     *
     * @param player le joueur qui execute la commande.
     * @param args   les arguments utilisées pour la commande.
     */
    protected List<String> tabCompleter(Player player, String[] args) {
        return Collections.emptyList();
    }

    protected void help(Player player, String usage) {
        player.spigot().sendMessage(TextComponent.fromLegacyText(error(usage)));
    }

    private String error(String usage) {
        return "  " + RED + "Should be used like" + AQUA + " - " + GRAY + usage;
    }

    public void showHelp(CommandSender sender) {
        List<Message> messages = new ArrayList<>();

        messages.add(new ChatMessage(DARK_GRAY + " *--------------------------------------------------*"));
        messages.add(ChatMessage.WHITE);
        messages.add(new ChatMessage(DefaultFontInfo.center(GRAY + "Help for §b- §r" + this.commandName)));
        messages.add(ChatMessage.WHITE);
        messages.add(new ChatMessage(buildSubcommand(YELLOW + "<help/h/?/>", "Show this message.")));
        messages.addAll(this.addHelp(sender));
        messages.add(ChatMessage.WHITE);
        messages.add(new ChatMessage(DARK_GRAY + " *--------------------------------------------------*"));

        messages.forEach(message -> message.send(sender));
    }

    protected abstract List<Message> addHelp(CommandSender sender);

    protected String buildSubcommand(String subcommand, String description, String... args) {
        String line;

        if (args != null && args.length != 0) {
            int           iMax = args.length - 1;
            StringBuilder b    = new StringBuilder();
            b.append(YELLOW).append('<');
            for (int i = 0; ; i++) {
                if (args[i].equalsIgnoreCase("on")) b.append(GREEN);
                else if (args[i].equalsIgnoreCase("off")) b.append(RED);

                b.append(args[i]);
                if (i == iMax) {
                    b.append(YELLOW).append('>');
                    break;
                }
                b.append(YELLOW).append('/');
            }
            line = AQUA + "  " + this.commandName + " " + WHITE + subcommand + " " + b.toString() + AQUA + " - " + GRAY + description;
        } else {
            line = AQUA + "  " + this.commandName + " " + WHITE + subcommand + " " + AQUA + "- " + GRAY + description;
        }

        return line;
    }

}
