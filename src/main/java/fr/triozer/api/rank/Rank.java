package fr.triozer.api.rank;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.scoreboard.Team;

import java.util.Set;

public interface Rank {

    // public static final CloudRank CREATOR = new CloudRank(-1, "Creator", ChatColor.LIGHT_PURPLE);

    void addPermission(Set<String> permissions);

    void addPermission(String... permissions);

    void removePermission(String... permissions);

    /**
     * @author https://stackoverflow.com/questions/40693462/java-string-matching-for-validating-permissions
     */
    default boolean hasPermission(String permission) {
        String regex = permission.replace(".", "\\.") // escape the dot in regex
                .replace("*", ".*") // prep the * token to match any 0+ chars
                .replace("?", ".")  // . will match any 1 char
                .replaceAll("\\[([^\\]\\[]*)]", "($1)") // [a,b] => (a|b) = match a or b
                .replaceAll("<([^<>]+)>", "(?:(?!$1)[^.])*") // <a,b> => (?:(?!a|b).)* = match text that is not a starting point of a or b
                .replace(",", "|"); // replace all commas with alternation symbol (for the above 2 patterns)

        return this.getPermissionsList().contains(permission)
                || this.getPermissionsList().stream().anyMatch(permissions -> permissions.matches(regex));
    }


    Set<String> getPermissionsList();

    int getID();

    String getName();

    String getPrefix();

    ChatColor getTag();

    Team getTeam();

    boolean showPrefix();

}

