package fr.triozer.api.rank;

import fr.triozer.api.manager.AbstractManager;
import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.api.property.DataProperty;
import fr.triozer.api.user.User;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author Cédric / Triozer
 */
public class Ranks {

    public static class Manager<R extends Rank> implements AbstractManager<R, Integer> {
        private final TrioPlugin plugin;
        private final Class<R>   rClass;

        private Scoreboard      scoreboard;
        private Map<R, Team>    teams;
        private Map<Integer, R> ranks;

        public Manager(TrioPlugin plugin, Class<R> rClass) {
            this.plugin = plugin;
            this.rClass = rClass;
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            this.teams = new HashMap<>();
            this.ranks = new HashMap<>();
            init();
        }

        private void init() {
            ConfigurationSection ranks = this.plugin.getConfiguration().create("ranks");

            if (ranks.getKeys(false).size() == 0) return;

            ranks.getKeys(false).forEach((key) -> {
                if (key.equals("allow-creator")) return;

                ConfigurationSection rank = ranks.getConfigurationSection(key);

                int          id          = rank.getInt("id");
                String       prefix      = rank.getString("name");
                ChatColor    color       = ChatColor.valueOf(rank.getString("color"));
                List<String> permissions = rank.getStringList("permissions");

                this.add(this.create(id, prefix, color, permissions));
                this.plugin.getConsole().fine("[R] Loaded `" + key + "` rank.");
            });
        }

        private R create(int id, String prefix, ChatColor color, List<String> permissions) {
            try {
                return this.rClass
                        .getConstructor(new Class[]{int.class, String.class, ChatColor.class, List.class})
                        .newInstance(id, prefix, color, permissions);
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
            }
            return null;
        }

        public void setRank(User player, R rank) {
            if (hasRank(player)) getRankOf(player).getTeam().removeEntry(player.getPlayer().getName());

            player.getProperty().set(this.plugin.getName().toLowerCase() + ".rank", new DataProperty.Value(rank.getID()));
            rank.getTeam().addEntry(player.getPlayer().getName());
        }

        public void setRank(User player, int rankID) {
            this.setRank(player, get(rankID));
        }

        public void setDefaultRank(User player) {
            this.setRank(player, get(0));
        }

        public boolean hasRank(User player) {
            return player.getConfiguration().contains(this.plugin.getName().toLowerCase() + ".rank");
        }

        public R getRankOf(User player) {
            return get(player.getConfiguration().getInt(this.plugin.getName().toLowerCase() + ".rank"));
        }

        @Override
        public R get(Integer key) {
            return this.ranks.get(key);
        }

        @Override
        public void add(R value) {
            R rank = get(value.getID());
            if (rank != null) {
                this.plugin.getConsole().error("[R] " + value.getID() + " is alreay used for " + rank.getName());
                return;
            }

            char c    = (char) (65 + value.getID()); // 0 = a, 1 = b...
            Team team = this.scoreboard.registerNewTeam("_" + c);

            if (!value.showPrefix()) {
                // on affiche pas le préfixe
                team.setPrefix(value.getTag() + "");
                team.setDisplayName(value.getTag() + "");
            } else {
                team.setPrefix(value.getPrefix());
                team.setDisplayName(value.getPrefix());
            }

            // active la visibilité du nom de team
            team.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.ALWAYS);

            // désactive les collisions
            team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);

            // ici on s'occupe de l'héritage des permission, le VIP aura les permissions du joueur et des permissions
            // supplémentaire ainsi de suite
            if (!this.ranks.isEmpty()) {
                values().forEach((next) -> {
                    if (value.getID() > next.getID()) {
                        // si la permission commence par un '$' alors cette permission ne doit pas être héritée
                        next.getPermissionsList().stream()
                                .filter(permission -> permission.startsWith("$") && !value.hasPermission(permission))
                                .forEach(value::addPermission);
                    }
                });
            }

            this.teams.put(value, team);
            this.ranks.put(value.getID(), value);
        }

        @Override
        public void remove(R value) {
            this.ranks.remove(value.getID());
        }

        @Override
        public Stream<R> values() {
            return this.ranks.values().stream();
        }

        public final Scoreboard getScoreboard() {
            return this.scoreboard;
        }

        public final Map<R, Team> getTeams() {
            return this.teams;
        }

        public final Map<Integer, R> getRanks() {
            return this.ranks;
        }

    }

}
