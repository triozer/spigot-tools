package fr.triozer.api.property;

import fr.triozer.api.database.mysql.Field;
import fr.triozer.core.TrioCore;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

public class DataProperty implements Property {

    public static final Value EMPTY = new Value(0);

    private UUID               owner;
    private Map<String, Value> values;
    private Field              where;

    private YamlConfiguration configuration;

    public DataProperty(UUID owner) {
        this.owner = owner;

        if (TrioCore.getInstance().getSettings().isSQL()) {
            TrioCore.USERS.insert(new Field("uuid", owner));
            this.where = new Field("uuid", owner);
        }

        this.values = new HashMap<>();
    }

    public DataProperty(UUID owner, YamlConfiguration configuration) {
        this.owner = owner;
        this.configuration = configuration;

        if (TrioCore.getInstance().getSettings().isSQL()) {
            TrioCore.USERS.insert(new Field("uuid", owner));
            this.where = new Field("uuid", owner);
        }

        this.values = new HashMap<>();

        for (String key : configuration.getKeys(false)) {
            if (configuration.isConfigurationSection(key)) {
                add(configuration.getConfigurationSection(key));
                continue;
            }

            Object value = configuration.get(key);
            Value  a     = new Value(value);

            if (value == null || String.valueOf(value).isEmpty() || String.valueOf(value).equals("0"))
                a = EMPTY;

            if (TrioCore.getInstance().getSettings().isSQL()) {
                if (a.getType().isEmpty())
                    TrioCore.USERS.add(key);
                else
                    TrioCore.USERS.add(key, a.getType());
            }

            this.values.put(key, a);
        }
    }

    private void add(ConfigurationSection section) {
        section.getKeys(false).forEach(key -> {
            if (section.isConfigurationSection(key)) {
                add(section);
                return;
            }

            Object value = section.get(key);
            Value  a     = new Value(value);


            if (value == null || String.valueOf(value).isEmpty() || String.valueOf(value).equals("0"))
                a = EMPTY;

            if (TrioCore.getInstance().getSettings().isSQL()) {
                if (a.getType().isEmpty())
                    TrioCore.USERS.add(key);
                else
                    TrioCore.USERS.add(key, a.getType());
            }

            this.values.put(section.getCurrentPath() + "." + key, a);
        });
    }

    @Override
    public void add(String key, int value) {
        if (get(key) == null) {
            if (TrioCore.getInstance().getSettings().isSQL()) TrioCore.USERS.add(key, "int");
            set(key, new Value(value));
            return;
        } else if (!(get(key).getValue() instanceof Integer)) return;

        get(key).set((int) get(key).getValue() + value);
    }

    @Override
    public void remove(String key) {
        this.values.remove(key);
    }

    @Override
    public void set(String key, Value value) {
        if (TrioCore.getInstance().getSettings().isSQL()) TrioCore.USERS.update(new Field(key, value), where);
        this.values.put(key, value);
    }

    @Override
    public Value get(String key) {
        return this.values.get(key);
    }

    @Override
    public void save() {
        this.values.forEach((key, value) -> {
            if (TrioCore.getInstance().getSettings().isSQL()) TrioCore.USERS.update(new Field(key, value), where);
            this.configuration.set(key, value.getValue());
        });

        try {
            this.configuration.save(TrioCore.getInstance().getConfiguration().getFile("datas/" + this.owner.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        this.values.forEach((key, value) -> builder.append(key).append("=").append(value.toString()).append(","));

        return "[" + builder.deleteCharAt(builder.length() - 1).toString() + "]";
    }

    @Override
    public final Stream<Value> values() {
        return this.values.values().stream();
    }

    @Override
    public final Stream<Map.Entry<String, Value>> set() {
        return this.values.entrySet().stream();
    }

    public final UUID getOwner() {
        return this.owner;
    }

}