package fr.triozer.api.property;

import java.util.Map;
import java.util.stream.Stream;

/**
 * @author Cédric / Triozer
 */
public interface Property {

    void add(String key, int value);

    void remove(String key);

    void set(String key, Value value);

    Value get(String key);

    void save();

    Stream<Value> values();

    Stream<Map.Entry<String, Value>> set();

    class Value {
        private Object value;
        private String type;

        public Value(Object value) {
            this.value = value;
            this.type = "";
        }

        public Value(String type, Object value) {
            this.value = value;
            this.type = type;
        }

        public Object getValue() {
            return this.value;
        }

        public void set(Object value) {
            this.value = value;
        }

        public void set(Value value) {
            this.value = value.value;
        }

        public String getType() {
            return this.type;
        }

        public boolean match(Object testValue) {
            return this.value != null && (this.value.equals(testValue) || this.value == testValue);
        }

        @Override
        public String toString() {
            return this.type + this.value.toString();
        }

    }


}
