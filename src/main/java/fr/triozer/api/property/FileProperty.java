package fr.triozer.api.property;

import fr.triozer.core.TrioCore;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class FileProperty implements Property {

    public static final Value EMPTY = new Value(0);

    private Map<String, Value> values;
    private YamlConfiguration  configuration;
    private String             name;

    public FileProperty(String name) {
        this.values = new HashMap<>();
        this.name = name;
        this.configuration = TrioCore.getInstance().getConfiguration().create(name);
    }

    public FileProperty(String name, YamlConfiguration configuration) {
        this.values = new HashMap<>();
        this.name = name;
        this.configuration = configuration;

        for (String key : configuration.getKeys(false)) {
            Object value = configuration.get(key);
            Value  a     = new Value(value);

            if (value == null || String.valueOf(value).isEmpty() || String.valueOf(value).equals("0"))
                a = EMPTY;

            this.values.put(key, a);
        }
    }


    @Override
    public void add(String key, int value) {
        if (get(key) == null) {
            set(key, new Value(value));
            return;
        } else if (!(get(key).getValue() instanceof Integer)) return;

        get(key).set((int) get(key).getValue() + value);
    }

    @Override
    public void remove(String key) {
        this.values.remove(key);
    }

    @Override
    public void set(String key, Value value) {
        this.values.put(key, value);
    }

    @Override
    public Value get(String key) {
        return this.values.get(key);
    }

    @Override
    public void save() {
        this.values.forEach((key, value) -> {
            configuration.set(key, value.toString());
        });

        try {
            configuration.save(TrioCore.getInstance().getConfiguration().getFile("datas/" + name));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        this.values.forEach((key, value) -> builder.append(key).append("=").append("" + value.getValue()).append(","));

        return "[" + builder.deleteCharAt(builder.length() - 1).toString() + "]";
    }

    @Override
    public final Stream<Value> values() {
        return this.values.values().stream();
    }

    @Override
    public final Stream<Map.Entry<String, Value>> set() {
        return this.values.entrySet().stream();
    }

    public final String getName() {
        return this.name;
    }

}