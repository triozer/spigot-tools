package fr.triozer.api.game;

/**
 * @author Cédric / Triozer
 */
public class Games {

    private String key;
    private String name;

    public Games(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public final String getKey() {
        return this.key;
    }

    public String getName() {
        return name;
    }

}
