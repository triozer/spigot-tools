package fr.triozer.api.game;

import fr.triozer.api.event.game.PlayerChangedWorldEvent;
import fr.triozer.api.game.description.GameDescription;
import fr.triozer.api.game.teleporter.GameTeleporter;
import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.api.property.FileProperty;
import fr.triozer.api.user.User;
import fr.triozer.core.TrioCore;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Triozer
 */
public abstract class Game {

    protected String prefix     = "§8[§bDefault§8] §r";
    protected int    minPlayers = 2;
    protected int    maxPlayers = 16;

    protected boolean start  = false;
    protected boolean finish = false;

    protected final TrioPlugin      plugin;
    protected final GameDescription description;
    protected final GameTeleporter  teleporter;
    protected final World           world;

    protected final List<User>   players;
    protected final List<User>   active;
    private final   FileProperty backup;

    private BukkitTask waitTask;
    private int        connected;

    public Game(TrioPlugin plugin, GameDescription description, GameTeleporter teleporter, World world) {
        this.prefix = prefix.replaceAll("Default", description.getName());

        this.plugin = plugin;
        this.description = description;
        this.teleporter = teleporter;
        this.world = world;

        this.players = new ArrayList<>(teleporter.getProperty().getMaxPlayers());
        this.active = new ArrayList<>(teleporter.getProperty().getMaxPlayers());
        this.backup = new FileProperty("backup", plugin.getConfiguration().create("backup"));

        TrioCore.getInstance().add(this);
    }

    public boolean isStarted() {
        return start;
    }

    /**
     * before start method
     */
    protected abstract void init();

    /**
     * This will run all the 20 ticks
     */
    protected abstract void update();

    protected abstract void start();

    protected abstract void finish();

    public void _start() {
        if (start) return;
        init();

        waitTask = new BukkitRunnable() {
            int timeBeforeStart = 15;

            @Override
            public void run() {
                update();

                if (players.size() < minPlayers) {
                    this.cancel();
                }

                if (timeBeforeStart == 0) {

                    active.addAll(players);

                    active.forEach(user -> {
                        Player player = user.getPlayer();

                        player.setLevel(0);
                        player.teleport(player.getWorld().getSpawnLocation());

                        player.getInventory().clear();
                    });

                    broadcastTranslatedCore("game.started");

                    connected = players.size();

                    start();
                    start = true;

                    teleporter.rebuild();

                    this.cancel();
                    return;
                }

                players.forEach(user -> user.getPlayer().setLevel(timeBeforeStart));

                if ((timeBeforeStart > 1 && timeBeforeStart <= 5) || timeBeforeStart % 10 == 0) {
                    broadcastTranslatedCore("game.start-plural", String.valueOf(timeBeforeStart));
                } else if (timeBeforeStart == 1) {
                    broadcastTranslatedCore("game.start", String.valueOf(timeBeforeStart));
                }

                timeBeforeStart--;
            }
        }.runTaskTimer(this.plugin, 20L, 20L);
    }

    public void _check() {
        if (active.size() != 1) return;
        if (finish) return;

        System.out.println("lol");

        finish = true;
        finish();

        int timeBeforeKick = 5;

        broadcastTranslatedCore("game.finished", String.valueOf(timeBeforeKick));

        new BukkitRunnable() {
            @Override
            public void run() {
                players.forEach(user ->
                        {
                            PlayerChangedWorldEvent event =
                                    new PlayerChangedWorldEvent(Game.this, user.getPlayer(), user.getPlayer().getWorld(), PlayerChangedWorldEvent.Reason.QUIT);
                            Bukkit.getPluginManager().callEvent(event);

                            if (event.isCancelled()) return;

                            user.getPlayer().teleport((Location) backup.get(user.getPlayer().getName() + ".location").getValue());
                        }
                );

                _delete();
            }
        }.runTaskLater(this.plugin, 20L * timeBeforeKick);
    }

    public boolean _empty() {
        int fDelete = 10;

        if (players.size() == 0) {
            this.plugin.getConsole().warning("[" + world.getName() + "] No active in this game. " + fDelete + " seconds before deleting!");
            new BukkitRunnable() {
                int delete = fDelete;

                @Override
                public void run() {
                    delete--;
                    if (players.size() > 0) this.cancel();
                    else if (delete == 0) _delete();
                }
            }.runTaskTimer(this.plugin, 0L, 20L);
            return true;
        }

        return false;
    }

    private void _delete() {
        String name = world.getName();
        Bukkit.unloadWorld(name, false);
        File parentFile = this.plugin.getDataFolder().getParentFile().getParentFile();
        deleteFile(new File(parentFile, name));

        teleporter.rebuild();
    }

    private void deleteFile(File element) {
        if (element.isDirectory() && element.listFiles() != null) {
            for (File sub : element.listFiles()) {
                deleteFile(sub);
            }
        }
        element.delete();
    }

    public void broadcast(String message) {
        this.players.forEach(user -> {
            Player player = user.getPlayer();

            if (player.getWorld() == this.world)
                player.sendMessage(message);
            else
                player.sendMessage(prefix + message);
        });
    }

    public void broadcastTranslatedCore(String path, String... replace) {
        this.players.forEach(user -> {
            Player player = user.getPlayer();

            if (player.getWorld() == this.world)
                user.sendTranslatedMessage(path, replace);
            else
                player.sendMessage(prefix + user.translate(path, replace));
        });
    }

    public void broadcastTranslated(String path, String... replace) {
        this.players.forEach(user -> {
            Player player = user.getPlayer();

            if (player.getWorld() == this.world)
                user.sendTranslatedMessage(plugin.getLanguage(), path, replace);
            else
                player.sendMessage(prefix + user.translate(plugin.getLanguage(), path, replace));
        });
    }


    public BukkitTask getWaitTask() {
        return waitTask;
    }

    public void remove(User user) {
        teleporter.remove(user.getPlayer());
        active.remove(user);
        user.setGame(null);
    }

    public void add(User user) {
        // teleporter.getQueue().remove(user.getPlayer());
        players.add(user);
        user.setGame(this);
    }

    public final List<User> players() {
        return this.players;
    }

    public final List<Player> getPlayers() {
        List<Player> list = new ArrayList<>();

        this.players.forEach(user -> list.add(user.getPlayer()));

        return list;
    }

    public final List<Player> getActive() {
        List<Player> list = new ArrayList<>();

        this.active.forEach(user -> list.add(user.getPlayer()));

        return list;
    }

    public final int getConnected() {
        return this.connected;
    }

    public GameDescription getDescription() {
        return this.description;
    }

    public final String getPrefix() {
        return this.prefix;
    }

    public final int getMinPlayers() {
        return this.minPlayers;
    }

    public final int getMaxPlayers() {
        return this.maxPlayers;
    }

    public final World getWorld() {
        return this.world;
    }

    public final String getMapFolder() {
        return this.plugin.getDataFolder() + "/maps/";
    }

    public void increment(Player killer) {
        TrioCore.getInstance().getPlayers().getUser(killer).getProperty().add("game." + teleporter.getID() + ".kills", 1);
    }

    public final FileProperty getBackup() {
        return this.backup;
    }

    public final GameTeleporter getTeleporter() {
        return this.teleporter;
    }

}
