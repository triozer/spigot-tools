package fr.triozer.api.game.description;

import fr.triozer.api.ui.ItemBuilder;
import org.bukkit.inventory.ItemStack;

/**
 * Cette classe est une description d'un mini-jeu.
 *
 * @author Cédric / Triozer
 */
public class GameDescription {

    private final String name;
    private final String version;
    private final String description;
    private final String author;

    private final ItemStack icon;

    /**
     * Crée une description pour un mini-jeu.
     *
     * @param name        le nom du mini-jeu.
     * @param version     la version du mini-jeu.
     * @param description la description du mini-jeu.
     * @param author      le développeur du mini-jeu.
     * @param icon        l'icône du mini-jeu.
     */
    private GameDescription(String name, String version, String description, String author, ItemStack icon) {
        this.name = name;
        this.version = version;
        this.description = description;
        this.author = author;

        this.icon = icon;
    }

    public final String getName() {
        return this.name;
    }

    public final String getVersion() {
        return this.version;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getAuthor() {
        return this.author;
    }

    public final ItemStack getIcon() {
        return this.icon;
    }

    /**
     * Créer la description du mini-jeu à l'aide d'un Constructeur.
     */
    public static class Builder {

        private String    name;
        private String    version;
        private String    description;
        private String    author;
        private ItemStack icon;

        public Builder(String name) {
            this.name = name;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder icon(ItemBuilder icon) {
            this.icon = icon.build();
            return this;
        }

        public Builder icon(ItemStack icon) {
            this.icon = icon;
            return this;
        }

        public GameDescription build() {
            return new GameDescription(name, version, description, author, icon);
        }

    }

}
