package fr.triozer.api.game.manager;

import fr.triozer.api.game.Game;
import fr.triozer.api.game.teleporter.GameTeleporter;
import org.bukkit.World;
import org.bukkit.block.Sign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Cette classe gère les arènes créées par le plugin du mini-jeu.
 *
 * @author Cédric / Triozer
 */
public class GameManager {

    private Map<String, GameTeleporter> teleporters;
    private Map<World, Game>            games;
    private Map<String, Sign>           teleportersSet;

    public GameManager() {
        this.teleporters = new HashMap<>();
        this.games = new HashMap<>();
        this.teleportersSet = new HashMap<>();
    }

    public void add(Game game) {
        this.games.put(game.getWorld(), game);
    }

    public void add(GameTeleporter teleporter) {
        this.teleporters.put(teleporter.getID(), teleporter);
    }

    public void add(String id, Sign sign) {
        this.teleportersSet.put(id, sign);
    }

    public void remove(Game game) {
        this.games.remove(game.getWorld());
    }

    public void remove(GameTeleporter teleporter) {
        this.teleporters.remove(teleporter.getID());
        this.teleportersSet.remove(teleporter.getID());
    }

    public void remove(Sign sign) {
        this.teleportersSet.remove(getID(sign));
    }

    public final Game getGame(World world) {
        return this.games.get(world);
    }

    public final GameTeleporter getTeleporter(Sign sign) {
        String id = getID(sign);
        return this.teleporters.get(id);
    }

    public final String getID(Sign sign) {
        String id = null;

        for (Map.Entry<String, Sign> entry : this.teleportersSet.entrySet()) {
            if (entry.getValue().equals(sign)) {
                id = entry.getKey();
            }
        }

        return id;
    }

    public final Stream<GameTeleporter> getTeleporters() {
        return this.teleporters.values().stream();
    }

    public final List<GameTeleporter> getTeleportersList() {
        return new ArrayList<>(this.teleporters.values());
    }

    public final Stream<Game> getGames() {
        return this.games.values().stream();
    }

    public final Stream<Sign> getTeleportersSet() {
        return this.teleportersSet.values().stream();
    }

}
