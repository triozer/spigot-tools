package fr.triozer.api.game.teleporter;

import fr.triozer.api.game.Game;
import fr.triozer.api.game.teleporter.queue.GameQueue;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

/**
 * Une interface qui permet de créer un téléporteur personnalisé pour chaque mini-jeu.
 *
 * @author Cédric / Triozer
 */
public interface GameTeleporter {

    /**
     * Ajouter un joueur à la queue du téléporteur.
     *
     * @param player le joueur à ajouter.
     */
    void add(Player player);

    /**
     * Mettre à jour la queue.
     */
    void update();

    /**
     * Enlever un joueur à la queue du téléporteur.
     *
     * @param player le joueur à enlever.
     */
    void remove(Player player);

    /**
     * Supprimer le téléporteur.
     */
    void destroy();

    /**
     * Reconstruire un téléporteur aux mêmes coordonnées et avec la même configuration.
     */
    void rebuild();

    /**
     * Créer une arène pour le mini-jeu.
     *
     * @param world
     */
    void createGame(World world);

    /**
     * Retourne le jeu lié au téléporteur.
     *
     * @return le jeu.
     */
    Game getCurrentGame();

    /**
     * Retourne la queue du téléporteur.
     *
     * @return la queue.
     */
    GameQueue getQueue();

    /**
     * Retourne l'identifiant du téléporteur.
     *
     * @return l'identifiant du téléporteur.
     */
    String getID();

    /**
     * Retourne le nom de la carte du téléporteur.
     *
     * @return le nom de la carte.
     */
    String getMapName();

    /**
     * Retourne la configuration du téléporteur.
     *
     * @return la configuration du téléporteur.
     */
    Property getProperty();

    /**
     * Désactiver l'utilisation du téléporteur.
     */
    void disable();

    /**
     * Retourne l'état du téléporteur.
     *
     * @return <code>true</code> si le téléporteur est utilisable.
     */
    boolean isActive();

    /**
     * La configuration d'un téléporteur.
     */
    class Property {
        private final int minPlayers;
        private final int maxPlayers;

        private final String map;

        /**
         * Créer une nouvelle configuration.
         *
         * @param maxPlayers le minimum de joueur.
         * @param minPlayers le maximum de joueur.
         */
        public Property(int minPlayers, int maxPlayers) {
            this.minPlayers = minPlayers;
            this.maxPlayers = maxPlayers;
            this.map = "Default";
        }

        /**
         * Créer une nouvelle configuration.
         *
         * @param maxPlayers le minimum de joueur.
         * @param minPlayers le maximum de joueur.
         * @param map        le nom de l'arène.
         */
        public Property(int minPlayers, int maxPlayers, String map) {
            this.minPlayers = minPlayers;
            this.maxPlayers = maxPlayers;
            this.map = map;
        }

        /**
         * Récupèrer une configuration à l'aide d'une configuration YAML.
         *
         * @param configuration la configuration YAML.
         * @return la configuration avec les valeurs de la configuration.
         */
        public Property load(YamlConfiguration configuration) {
            return new Property(configuration.getInt("teleporter.min-active"), configuration.getInt("teleporter.max-active"));
        }

        public final int getMinPlayers() {
            return this.minPlayers;
        }

        public final int getMaxPlayers() {
            return this.maxPlayers;
        }

        public final String getMap() {
            return this.map;
        }
    }

}
