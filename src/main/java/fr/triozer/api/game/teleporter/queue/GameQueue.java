package fr.triozer.api.game.teleporter.queue;

import fr.triozer.api.game.teleporter.GameTeleporter;
import fr.triozer.core.TrioCore;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe représente le système de queue utilisé pour les mini-jeu.
 *
 * @author Cédric / Triozer
 */
public class GameQueue {

    private List<Player>   players;
    private GameTeleporter parent;

    /**
     * Crée une queue en fonction d'un {@link fr.triozer.api.game.teleporter.GameTeleporter}.
     *
     * @param parent le téléporteur.
     */
    public GameQueue(GameTeleporter parent) {
        players = new ArrayList<>();
        this.parent = parent;
    }

    /**
     * Ajoute un joueur dans la queue.
     *
     * @param player le joueur à ajouter à la queue.
     */
    public void add(Player player) {
        this.players.add(player);

        TrioCore.getInstance().getPlayers().getUser(player).sendTranslatedMessage("queue.join", this.parent.getID());
    }

    /**
     * Vérifie si le joueur est dans la queue
     *
     * @param player le joueur à vérifier.
     * @return Retourne <code>true</code> si le joueur est dans la queue.
     */
    public boolean contains(Player player) {
        return this.players.contains(player);
    }

    /**
     * Supprime la queue dans le silence. Aucune annonce.
     */
    public void destroySilent() {
        this.players.clear();
        this.parent = null;
    }

    /**
     * Supprime la queue en faisant une annonc aux joueurs.
     */
    public void destroy() {
        destroySilent();
        this.players.forEach(player ->
                TrioCore.getInstance().getPlayers().getUser(player).sendTranslatedMessage("queue.delete", parent.getID()));
    }

    /**
     * Enlève un joueur de la queue.
     *
     * @param player le joueur à enlever à la queue.
     */
    public void remove(Player player) {
        this.players.remove(player);

        TrioCore.getInstance().getPlayers().getUser(player).sendTranslatedMessage("queue.leave", parent.getID());
    }

    /**
     * Retourne le premier joueur de la queue.
     *
     * @return Le premier joueur de la queue.
     */
    public Player first() {
        return this.players.get(0);
    }

    /**
     * Retourne le dernier joueur de la queue.
     *
     * @return Le dernier joueur de la queue.
     */
    public Player last() {
        return this.players.get(this.players.size());
    }

    /**
     * Retourne le nombre de joueurs dans la queue.
     *
     * @return Le nombre de joueurs dans la queue.
     */
    public final int getSize() {
        return this.players.size();
    }

    /**
     * Retourne la liste des joueurs dans la queue.
     *
     * @return La liste des joueurs dans la queue.
     */
    public final List<Player> getPlayers() {
        return this.players;
    }

    /**
     * Retourne le téléporteur lié avec la queue.
     *
     * @return Le téléporteur.
     */
    public final GameTeleporter getParent() {
        return this.parent;
    }
}
