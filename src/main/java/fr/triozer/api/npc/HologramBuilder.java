package fr.triozer.api.npc;

import fr.triozer.core.TrioCore;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Cédric / Triozer
 */
public class HologramBuilder {

    private Map<Integer, ArmorStand> armorStands;
    private Map<Integer, String>     lines;
    private Location                 location;
    private Item                     item;
    private int time = -1;

    public HologramBuilder(Location location) {
        this.armorStands = new HashMap<>();
        this.lines = new HashMap<>();
        this.location = location;
    }

    public HologramBuilder lines(String... lines) {
        for (int i = 0; i < lines.length; i++)
            this.lines.put(i, lines[i]);

        return this;
    }

    public HologramBuilder time(int time) {
        this.time = time;

        return this;
    }

    public HologramBuilder item(ItemStack itemStack) {
        this.item = location.getWorld().dropItem(getLocation(), itemStack);
        item.setPickupDelay(Integer.MAX_VALUE);

        return this;
    }

    public HologramBuilder build() {
        if (item == null) for (int i = 0; i < lines.size(); i++) set(i, lines.get(i));
        else {
            ArmorStand armorStand = (ArmorStand) location.getWorld().spawnEntity(location.subtract(0, 0.25, 0), EntityType.ARMOR_STAND);

            armorStand.setVisible(false);
            armorStand.setGravity(false);

            armorStand.setPassenger(item);

            armorStands.put(0, armorStand);
        }

        if (time != -1)
            Bukkit.getScheduler().runTaskLater(TrioCore.getInstance(), this::end, 20L * time);

        return this;
    }

    public void end() {
        armorStands.values().forEach(Entity::remove);

        if (item != null)
            item.remove();
    }

    public void update(int line, String text) {
        ArmorStand armorStand = armorStands.get(line);

        if (armorStand == null) return;

        armorStand.setCustomNameVisible(false);

        if (!"".equals(text)) {
            armorStand.setCustomName(text);
            armorStand.setCustomNameVisible(true);
        }
    }

    public List<ArmorStand> getArmorStands() {
        return (List<ArmorStand>) armorStands.values();
    }

    public Location getLocation() {
        return location;
    }

    public String[] getLines() {
        return (String[]) lines.values().toArray();
    }

    public Item getItem() {
        return item;
    }

    public void set(int i, String text) {
        this.lines.put(i, text);

        ArmorStand armorStand;

        if (this.armorStands.get(i) == null) {
            armorStand = (ArmorStand) location.getWorld().spawnEntity(location.subtract(0, 0.25, 0), EntityType.ARMOR_STAND);

            armorStand.setVisible(false);
            armorStand.setGravity(false);
            armorStand.setCustomNameVisible(false);
        } else
            armorStand = this.armorStands.get(i);

        if (!"".equals(lines.get(i))) {
            armorStand.setCustomName(lines.get(i));
            armorStand.setCustomNameVisible(true);
        }

        armorStands.put(i, armorStand);
    }

}
