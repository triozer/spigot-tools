package fr.triozer.api.npc;

import fr.triozer.api.entity.TrioEntity;
import fr.triozer.api.util.Updatable;
import fr.triozer.core.TrioCore;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.UUID;
import java.util.function.BiConsumer;

/**
 * @author Cédric / Triozer
 */
public class NpcBuilder extends TrioEntity<Villager> implements Updatable {

    private String   id;
    private String   name;
    private String[] lines;
    private boolean  update;

    private Villager.Profession profession;

    private Location location;

    private HologramBuilder hologramBuilder;

    private NpcListener listener;

    public NpcBuilder(Location location, boolean update) {
        super(EntityType.VILLAGER);

        this.update = update;

        this.id = UUID.randomUUID().toString().substring(4);
        this.location = location;
    }

    public NpcBuilder name(String... lines) {
        this.lines = new String[lines.length];
        for (int i = 0; i < lines.length; i++)
            this.lines[i] = lines[i];

        return this;
    }

    public NpcBuilder name(HologramBuilder hologramBuilder) {
        this.hologramBuilder = hologramBuilder;

        return this;
    }

    public NpcBuilder profession(Villager.Profession profession) {
        this.profession = profession;

        return this;
    }

    public NpcBuilder addListener(NpcListener listener) {
        this.listener = listener;

        return this;
    }

    public NpcBuilder setName(String name) {
        this.name = name;

        return this;
    }

    public NpcBuilder build() {
        entity = (Villager) location.getWorld().spawnEntity(location, EntityType.VILLAGER);

        setNoAI(entity);
        entity.setCollidable(false);

        if (profession == null)
            entity.setProfession(Villager.Profession.LIBRARIAN);
        else
            entity.setProfession(profession);

        if (hologramBuilder == null)
            hologramBuilder = new HologramBuilder(entity.getLocation().add(0, lines.length * 0.25, 0))
                    .lines(lines).build();

        Bukkit.getPluginManager().registerEvents(new NpcListeners(), TrioCore.getInstance());

        return this;
    }

    public void set(int i, String text) {
        hologramBuilder.set(i, text);
    }

    public void update() {
        if (!update) return;
    }

    @Override
    public final boolean isUpdatable() {
        return this.update;
    }

    @Override
    public void end() {
        hologramBuilder.end();

        if (!entity.isDead())
            entity.remove();
    }

    private void setNoAI(Villager villager) {
        villager.setAI(false);
        villager.setSilent(true);
    }

    public void sendMessageTo(Player player, String message) {
        player.sendMessage("§b" + name + "§f: " + message);
    }

    public final String getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final String[] getLines() {
        return this.lines;
    }

    public final Location getLocation() {
        return this.location;
    }

    public static abstract class NpcListener {
        public abstract BiConsumer<Player, PlayerInteractEntityEvent> click();
    }

    private final class NpcListeners implements Listener {
        @EventHandler
        public void onDeath(EntityDeathEvent event) {
            if (event.getEntity() instanceof Villager && event.getEntity() == entity) {
                end();
            }
        }

        @EventHandler
        public void onHit(EntityDamageByEntityEvent event) {
            if (event.getEntity() instanceof Villager && event.getEntity() == entity) {
                event.setCancelled(true);
            }
        }

        @EventHandler
        public void onClick(PlayerInteractEntityEvent event) {
            if (event.getRightClicked() instanceof Villager && event.getRightClicked() == entity) {
                if (listener != null)
                    listener.click().accept(event.getPlayer(), event);

                event.setCancelled(true);
            }
        }
    }
}