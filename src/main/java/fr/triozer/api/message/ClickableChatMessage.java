package fr.triozer.api.message;

import fr.triozer.api.util.TextBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;

/**
 * @author Cédric / Triozer
 */
public class ClickableChatMessage implements Message {

    private TextComponent[] components;

    public ClickableChatMessage(TextComponent... components) {
        this.components = components;
    }

    public ClickableChatMessage(TextBuilder... builders) {
        this.components = new TextComponent[builders.length];

        for (int i = 0; i < builders.length; i++) this.components[i] = builders[i].build();
    }

    @Override
    public void send(CommandSender target) {
        target.spigot().sendMessage(this.components);
    }

    @Override
    public Type getType() {
        return Type.CLICKABLE;
    }

    public final TextComponent[] getComponents() {
        return this.components;
    }

}
