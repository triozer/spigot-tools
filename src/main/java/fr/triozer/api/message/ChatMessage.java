package fr.triozer.api.message;

import fr.triozer.api.i18n.I18N;
import org.bukkit.command.CommandSender;

/**
 * @author Cédric / Triozer
 */
public class ChatMessage implements Message {

    public static final Message WHITE = new ChatMessage("");

    private String text;

    public ChatMessage(String text) {
        this.text = text;
    }

    public ChatMessage(I18N lang, String path, String... replacement) {
        this.text = lang.translate(path, replacement);
    }

    @Override
    public void send(CommandSender target) {
        target.sendMessage(text);
    }

    @Override
    public Type getType() {
        return Type.CHAT;
    }

    public final String getText() {
        return this.text;
    }

}
