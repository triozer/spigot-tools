package fr.triozer.api.message;

import org.bukkit.command.CommandSender;

/**
 * Cette classe est le modèle de base pour l'envoi de message à l'aide de différentes méthodes.
 *
 * @author Cédric / Triozer
 */
public interface Message {

    /**
     * Envoyer le message à une entité capable de l'intérpréter.
     *
     * @param target l'entité.
     */
    void send(CommandSender target);

    /**
     * Rétourne la méthode de l'envoi du message.
     *
     * @return la méthode de l'envoi du message.
     */
    Type getType();

    /**
     * Une énumération des différentes méthodes.
     */
    enum Type {
        /**
         * Envoi de message par l'action bar.
         */
        ACTIONBAR,
        /**
         * Envoi de message par le chat.
         */
        CHAT,
        /**
         * Envoi d'un message cliquable.
         */
        CLICKABLE,
        /**
         * Envoi de message sous forme de titre.
         */
        TITLE
    }

}
