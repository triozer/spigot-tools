package fr.triozer.api.message;

import fr.triozer.api.util.lang.ServerPackage;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;

/**
 * @author Cédric / Triozer
 */
public class TitleMessage implements Message {

    private String title;
    private String subtitle;

    private final int stay;
    private final int fadeIn;
    private final int fadeOut;

    public TitleMessage(String title) {
        this(title, "");
    }

    public TitleMessage(String title, String subtitle) {
        this(title, subtitle, 5);
    }

    public TitleMessage(String title, String subtitle, int time) {
        this(title, subtitle, time, 2, 2);
    }

    public TitleMessage(String title, String subtitle, int time, int fadeIn, int fadeOut) {
        this.title = title;
        this.subtitle = subtitle;
        this.stay = time;
        this.fadeIn = fadeIn;
        this.fadeOut = fadeOut;
    }

    @Override
    public void send(CommandSender target) {
        Player player = (Player) target;

        if (player == null || !player.isOnline()) {
            return;
        }

        try {
            Object      e;
            Object      chatTitle;
            Object      chatSubtitle;
            Constructor subtitleConstructor;
            Object      titlePacket;
            Object      subtitlePacket;

            if (title != null) {
                title = ChatColor.translateAlternateColorCodes('&', title);
                title = title.replaceAll("%player%", player.getDisplayName());
                // Times packets
                e = ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle");

                e = ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TIMES").get(null);
                chatTitle = ServerPackage.MINECRAFT.getClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + title + "\"}");
                subtitleConstructor = ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getConstructor(ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getDeclaredClasses()[0], ServerPackage.MINECRAFT.getClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE);
                titlePacket = subtitleConstructor.newInstance(e, chatTitle, fadeIn, stay, fadeOut);
                sendPacket(player, titlePacket);

                e = ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get(null);
                chatTitle = ServerPackage.MINECRAFT.getClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + title + "\"}");
                subtitleConstructor = ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getConstructor(ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getDeclaredClasses()[0], ServerPackage.MINECRAFT.getClass("IChatBaseComponent"));
                titlePacket = subtitleConstructor.newInstance(e, chatTitle);
                sendPacket(player, titlePacket);
            }

            if (subtitle != null) {
                subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
                subtitle = subtitle.replaceAll("%player%", player.getDisplayName());
                // Times packets
                e = ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TIMES").get(null);
                chatSubtitle = ServerPackage.MINECRAFT.getClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + title + "\"}");
                subtitleConstructor = ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getConstructor(ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getDeclaredClasses()[0], ServerPackage.MINECRAFT.getClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE);
                subtitlePacket = subtitleConstructor.newInstance(e, chatSubtitle, fadeIn, stay, fadeOut);
                sendPacket(player, subtitlePacket);

                e = ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("SUBTITLE").get(null);
                chatSubtitle = ServerPackage.MINECRAFT.getClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + subtitle + "\"}");
                subtitleConstructor = ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getConstructor(ServerPackage.MINECRAFT.getClass("PacketPlayOutTitle").getDeclaredClasses()[0], ServerPackage.MINECRAFT.getClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE);
                subtitlePacket = subtitleConstructor.newInstance(e, chatSubtitle, fadeIn, stay, fadeOut);
                sendPacket(player, subtitlePacket);
            }
        } catch (Exception var11) {
            var11.printStackTrace();
        }
    }

    @Override
    public Type getType() {
        return Type.TITLE;
    }

    public final String getTitle() {
        return this.title;
    }

    public final String getSubtitle() {
        return this.subtitle;
    }

    public final int getStay() {
        return this.stay;
    }

    public final int getFadeIn() {
        return this.fadeIn;
    }

    public final int getFadeOut() {
        return this.fadeOut;
    }

    private void sendPacket(Player player, Object packet) {
        try {
            Object handle           = player.getClass().getMethod("getHandle").invoke(player);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", ServerPackage.MINECRAFT.getClass("Packet")).invoke(playerConnection, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
