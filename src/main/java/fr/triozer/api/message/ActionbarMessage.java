package fr.triozer.api.message;

import fr.triozer.api.util.lang.ServerPackage;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Cédric / Triozer
 */
public class ActionbarMessage implements Message {

    private String text;

    public ActionbarMessage(String text) {
        this.text = text;
    }

    @Override
    public void send(CommandSender target) {
        Player player = (Player) target;

        try {
            Class<?> clsIChatBaseComponent = ServerPackage.MINECRAFT.getClass("IChatBaseComponent");
            Class<?> clsChatMessageType    = ServerPackage.MINECRAFT.getClass("ChatMessageType");
            Object   entityPlayer          = player.getClass().getMethod("getHandle").invoke(player);
            Object   playerConnection      = entityPlayer.getClass().getField("playerConnection").get(entityPlayer);
            Object   chatBaseComponent     = ServerPackage.MINECRAFT.getClass("IChatBaseComponent$ChatSerializer").getMethod("a", String.class).invoke(null, "{\"text\":\"" + text + "\"}");
            Object   chatMessageType       = clsChatMessageType.getMethod("valueOf", String.class).invoke(null, "GAME_INFO");
            Object   packetPlayOutChat     = ServerPackage.MINECRAFT.getClass("PacketPlayOutChat").getConstructor(clsIChatBaseComponent, clsChatMessageType).newInstance(chatBaseComponent, chatMessageType);
            playerConnection.getClass().getMethod("sendPacket", ServerPackage.MINECRAFT.getClass("Packet")).invoke(playerConnection, packetPlayOutChat);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Type getType() {
        return Type.ACTIONBAR;
    }

    public final String getText() {
        return this.text;
    }

}
