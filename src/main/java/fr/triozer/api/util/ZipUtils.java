package fr.triozer.api.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipUtils {
    private static final int BUFFER_SIZE = 4096;

    /**
     * Extract zipfile to outdir with complete directory structure
     *
     * @param zipfile Input .zip file
     * @param outdir  Output directory
     */
    public static void extract(File zipfile, File outdir) throws IOException {

        ZipFile        zipFile = new ZipFile(zipfile);
        Enumeration<?> enu     = zipFile.entries();
        while (enu.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) enu.nextElement();

            String name           = zipEntry.getName();
            long   size           = zipEntry.getSize();
            long   compressedSize = zipEntry.getCompressedSize();

            File file = new File(outdir, name);
            if (name.endsWith("/")) {
                file.mkdirs();
                continue;
            }

            File parent = file.getParentFile();
            if (parent != null) {
                parent.mkdirs();
            }

            InputStream      is    = zipFile.getInputStream(zipEntry);
            FileOutputStream fos   = new FileOutputStream(file);
            byte[]           bytes = new byte[1024];
            int              length;
            while ((length = is.read(bytes)) >= 0) {
                fos.write(bytes, 0, length);
            }
            is.close();
            fos.close();

        }
        zipFile.close();

    }

}