package fr.triozer.api.util;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Triozer
 */
public class Particles {

    public static void center(Player player, Particle particle) {
        World world = player.getWorld();
        double increment = (2 * Math.PI) / 360;
        List<Location> locations = new ArrayList<>();

        for (int i = 0; i < 360 * 2; i++) {
            double angle = i * increment;
            double x = player.getLocation().getX() + (2 * Math.cos(angle));
            double z = player.getLocation().getZ() + (2 * Math.sin(angle));

            locations.add(new Location(world, x, player.getLocation().getY(), z));
        }

        for (Location location : locations) {
            player.spawnParticle(particle, location, 100);
        }
    }

    public static void helix(Player player, Particle particle) {
        int radius = 2;

        for (double y = 0; y <= 50; y += 0.05) {
            double x = radius * Math.cos(y);
            double z = radius * Math.sin(y);
    
            player.spawnParticle(particle, x, y, z, 100);
        }
    }

}
