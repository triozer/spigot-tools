package fr.triozer.api.util;

/**
 * Add possibility to update object who implements this.
 *
 * @author Cédric / Triozer
 */
public interface Updatable {

    /**
     * Update method.
     */
    void update();

    boolean isUpdatable();

}
