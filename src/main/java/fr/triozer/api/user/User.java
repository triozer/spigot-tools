package fr.triozer.api.user;

import fr.triozer.api.game.Game;
import fr.triozer.api.i18n.I18N;
import fr.triozer.api.message.ChatMessage;
import fr.triozer.api.property.DataProperty;
import fr.triozer.api.rank.Rank;
import fr.triozer.api.rank.Ranks;
import fr.triozer.api.scoreboard.IndividualScoreboard;
import fr.triozer.core.TrioCore;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * @author Cédric / Triozer
 */
public class User {

    private UUID         id;
    private DataProperty property;

    private Game game;

    private IndividualScoreboard scoreboard;

    public User(UUID id, DataProperty property) {
        this.id = id;
        this.property = property;

        if (this.property.get("last") == null) {
            this.property.set("language", new DataProperty.Value(TrioCore.getInstance().getSettings().getLanguage().getCode()));
            this.property.set("last", new DataProperty.Value(System.currentTimeMillis()));
            this.property.set("triocore.rank", new DataProperty.Value("0"));
        }

        this.property.set("last", new DataProperty.Value(System.currentTimeMillis()));
        this.game = null;
        this.property.save();
    }

    public void sendTranslatedMessage(String path, String... replace) {
        new ChatMessage(translate(path, replace)).send(getPlayer());
    }

    public void sendTranslatedMessage(I18N.Manager manager, String path, String... replace) {
        new ChatMessage(translate(manager, path, replace)).send(getPlayer());
    }

    public String translate(String path, String... replace) {
        return getLocale(TrioCore.getInstance().getLanguage()).translate(path, replace);
    }

    public String translate(I18N.Manager manager, String path, String... replace) {
        return getLocale(manager).translate(path, replace);
    }

    public final UUID getID() {
        return this.id;
    }

    public final DataProperty getProperty() {
        return this.property;
    }

    public final Player getPlayer() {
        return Bukkit.getPlayer(this.id);
    }

    public final boolean isInGame() {
        return this.game != null;
    }

    public final Game getGame() {
        return this.game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public final I18N getLocale(I18N.Manager manager) {
        return manager.get((String) this.property.get("language").getValue());
    }

    public void setLocale(I18N locale) {
        this.property.set("language", new DataProperty.Value(locale.getCode()));
    }

    public YamlConfiguration getConfiguration() {
        return TrioCore.getInstance().getConfiguration().get("datas/" + id);
    }

    public final IndividualScoreboard getScoreboard() {
        return this.scoreboard;
    }

    public void setScoreboard(IndividualScoreboard scoreboard) {
        this.scoreboard = scoreboard;
        this.scoreboard.make();
    }

    public ConfigurationSection write(ConfigurationSection section) {
        section.set("id", this.id);
        section.set("name", this.getPlayer().getDisplayName());

        return section;
    }

    public Rank getRank(Ranks.Manager manager) {
        return manager.getRankOf(this);
    }

    public Rank getRank() {
        return TrioCore.getInstance().getRanks().getRankOf(this);
    }

    public boolean hasPermission(String permission) {
        return getRank().hasPermission(permission);
    }

}
