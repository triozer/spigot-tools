package fr.triozer.api.event.game;

import fr.triozer.api.game.Game;
import org.bukkit.entity.Player;

/**
 * Cette classe représente un événement actionné par un joueur et lié aux mini-jeux.
 *
 * @author Cédric / Triozer
 */
public class PlayerGameEvent extends GameEvent {

    protected Player player;

    public PlayerGameEvent(Game game, Player who) {
        super(game);
        this.player = who;
    }

    /**
     * Récupère le joueur ayant actionné l'événément.
     *
     * @return Le {@link org.bukkit.entity.Player} ayant actionné l'événément.
     */
    public final Player getPlayer() {
        return this.player;
    }

}
