package fr.triozer.api.event.game;

import fr.triozer.api.game.Game;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;

/**
 * Appelé lorsqu'un joueur change de monde.
 *
 * @author Cédric / Triozer
 */
public class PlayerChangedWorldEvent extends PlayerGameEvent implements Cancellable {

    private final World   from;
    private final Reason  reason;
    private       boolean cancelled;

    /**
     * Le constructeur principal.
     *
     * @param game   le {@link fr.triozer.api.game.Game} jeu en rapport avec l'événement.
     * @param player le {@link org.bukkit.entity.Player} joueur ayant changé de monde.
     * @param from   le {@link org.bukkit.World} monde d'où vient le joueur.
     * @param reason la {@link Reason} raison de la téléportation.
     */
    public PlayerChangedWorldEvent(Game game, Player player, World from, Reason reason) {
        super(game, player);

        this.from = from;
        this.reason = reason;
    }

    /**
     * Récupère la {@link Reason} raison de la téléportation.
     *
     * @return La {@link Reason} raison de la téléportation.
     */
    public final Reason getReason() {
        return this.reason;
    }

    /**
     * Récupère le {@link org.bukkit.World} monde d'où vient le joueur.
     *
     * @return Le {@link org.bukkit.World} monde d'où vient le joueur.
     */
    public World getFrom() {
        return this.from;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    /**
     * Une énumération des deux seules raisons provoquant la téléportation du joueur.
     */
    public enum Reason {
        /**
         * Le joueur est téléporté parce qu'il rejoint le monde.
         */
        JOIN,
        /**
         * Le joueur est téléporté parce qu'il quitte le monde.
         */
        QUIT
    }

}
