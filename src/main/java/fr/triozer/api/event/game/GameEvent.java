package fr.triozer.api.event.game;

import fr.triozer.api.game.Game;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Cette classe représente un événement lié aux mini-jeux.
 *
 * @author Cédric / Triozer
 */
public class GameEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final Game game;

    public GameEvent(Game game) {
        this.game = game;
    }

    /**
     * Récupèrer le jeu en rapport avec cet événement.
     *
     * @return Le {@link fr.triozer.api.game.Game} en rapport avec cet événement.
     */
    public final Game getGame() {
        return this.game;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
