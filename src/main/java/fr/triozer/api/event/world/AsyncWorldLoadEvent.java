package fr.triozer.api.event.world;

import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.event.HandlerList;
import org.bukkit.event.world.WorldEvent;

/**
 * @author Cédric / Triozer
 */
public class AsyncWorldLoadEvent extends WorldEvent {

    private static final HandlerList handlers = new HandlerList();

    public static HandlerList getHandlersList() {
        return handlers;
    }

    public AsyncWorldLoadEvent(CraftWorld world) {
        super(world);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

}
