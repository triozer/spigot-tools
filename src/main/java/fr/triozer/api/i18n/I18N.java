package fr.triozer.api.i18n;

import fr.triozer.api.manager.AbstractManager;
import fr.triozer.core.TrioCore;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Cette classe gère le support multi-langue des plugins.
 *
 * @author Cédric / Triozer
 */
public class I18N {

    private String            lang;
    private String            code;
    private YamlConfiguration file;

    /**
     * Créer une nouvelle langue à parti d'une configuration YAML.
     *
     * @param lang le nom de la langue.
     * @param code le code utilisé pour la lanque.
     * @param yaml la configuration YAML.
     */
    public I18N(String lang, String code, YamlConfiguration yaml) {
        this.lang = lang;
        this.code = code;
        this.file = yaml;
    }

    /**
     * Retourne le texte traduit dans cette langue.
     *
     * @param path    le chemin vers cette traduction.
     * @param replace les mots à remplacer.
     * @return le texte traduit.
     */
    public String translate(String path, String... replace) {
        if (this.file == null)
            return "§cCan't traduce §l" + path + " §cbecause file is null.";

        if (!this.file.contains(path))
            return "[Translator] Hein? '" + path + "'";

        String translate = ChatColor.translateAlternateColorCodes('&', this.file.getString(path));

        for (int i = 0; i < replace.length; i++)
            translate = translate.replace("{" + i + "}", replace[i]);

        return translate;
    }

    /**
     * Retourne la langue.
     *
     * @return la langue.
     */
    public final String getLang() {
        return this.lang;
    }

    /**
     * Retourne le code.
     *
     * @return le code.
     */
    public final String getCode() {
        return this.code;
    }

    /**
     * Retourne la configuration YAML.
     *
     * @return la configuration YAML.
     */
    public final YamlConfiguration getFile() {
        return this.file;
    }

    @Override
    public String toString() {
        return this.lang;
    }

    /**
     * Cette classe gère l'implémentation des langues dans les plugins.
     */
    public static class Manager implements AbstractManager<I18N, String> {

        private final Map<String, I18N> languages;

        /**
         * Créer un nouveau manager.
         */
        public Manager() {
            this.languages = new HashMap<>();
        }

        @Override
        public I18N get(String key) {
            return this.languages.get(key);
        }

        @Override
        public void add(I18N value) {
            this.languages.putIfAbsent(value.getCode(), value);
        }

        @Override
        public void remove(I18N value) {
            this.languages.remove(value.getCode());
        }

        @Override
        public Stream<I18N> values() {
            return this.languages.values().stream();
        }

        /**
         * Retourne le texte traduit dans la langue spécifiée.
         *
         * @param language la langue à traduire.
         * @param path     le chemin vers cette traduction.
         * @param replace  les mots à remplacer.
         * @return le texte traduit.
         */
        public static String translate(I18N language, String path, String... replace) {
            return language.translate(path, replace);
        }

    }

}