package fr.triozer.api.manager;

import java.util.stream.Stream;

/**
 * Abstract generic manager
 *
 * @param <K> value
 * @param <V> key
 * @author Cédric / Triozer
 */
public interface AbstractManager<K, V> {

    /**
     * Get registered generic value
     *
     * @param key key
     * @return registered value
     */
    K get(V key);

    /**
     * Add given generic value
     *
     * @param value a generic value
     */
    void add(K value);

    /**
     * Add multiple given generic values
     *
     * @param values generic values
     */
    default void addAll(K... values) {
        Stream.of(values).forEach(this::add);
    }

    /**
     * Remove given generic parameter
     *
     * @param value a generic value
     */
    void remove(K value);

    /**
     * Remove multiple given generic values
     *
     * @param values generic values
     */
    default void removeAll(K... values) {
        Stream.of(values).forEach(this::remove);
    }

    /**
     * Get all values {@link AbstractManager#add(Object)}
     *
     * @return a {@link Stream} of all values
     */
    Stream<K> values();

}