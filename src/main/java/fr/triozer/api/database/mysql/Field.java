package fr.triozer.api.database.mysql;

/**
 * This class represent an object from a mysql database.
 *
 * @author Cédric / Triozer
 */
public class Field {

    private final String name;
    private final Object value;

    /**
     * Principal constructor.
     *
     * @param name  the name of the field.
     * @param value the value of the field.
     */
    public Field(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Retrieves the parsed name with the simple quotes.
     *
     * @return The parsed name with the simple quotes.
     */
    public String getParsedName() {
        return " `" + this.name + "` ";
    }

    /**
     * Retrieves the parsed value with the simple quotes.
     *
     * @return The parsed value with the simple quotes.
     */
    public String getParsedValue() {
        return " '" + this.value + "' ";
    }

    /**
     * Retrieves the parsed name and values with
     * simple quotes and equal.
     *
     * @return The parsed name and values with simple quotes and equal.
     */
    public String getParsedNameAndValue() {
        return getParsedName() + "=" + getParsedValue();
    }


}
