package fr.triozer.api.database.mysql;

/**
 * @author Cédric / Triozer
 */
public enum Method {

    SELECT, WHERE, FROM, INSERT_INTO, VALUES, UPDATE, DELETE_FROM, SET, AND;

    /**
     * Retrieves the name of the method.
     *
     * @return The name of the method.
     */
    @Override
    public String toString() {
        return name().replace("_", " ");
    }

}
