package fr.triozer.api.database.mysql;

import fr.triozer.api.util.Console;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe est la classe utilisée pour la liaison d'un plugin à une base de donnée.
 *
 * @author Cédric / Triozer
 */
public class MySQL {

    private final Console console;

    private final String database;
    private final String host;
    private final int    port;

    private Connection connection;
    private boolean    connected;

    /**
     * Le constructeur principal.
     *
     * @param console  {@link fr.triozer.api.util.Console} la 'sortie' de la console.
     * @param host     l'adresse de la base de donnée.
     * @param port     le port de la base de donnée.
     * @param database le nom de la base de donnée.
     */
    public MySQL(Console console, String host, int port, String database) {
        this.console = console;
        this.database = database;
        this.host = host;
        this.port = port;
    }

    /**
     * Cette méthode gère la connexion à la base donnée.
     * <p>
     * Les informations utilisées dans les paramètres ne sont jamais enregistrés.
     *
     * @param username le nom d'utilisateur utilisé pour la connexion.
     * @param password le mot de passse de l'utilisateur.
     * @return la classe avec l'objet {@link java.sql.Connection} initialisé.
     */
    public MySQL auth(String username, String password) {
        if (connected)
            return this;

        try {
            this.connection = DriverManager
                    .getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?autoReconnect=true&useSSL=false"
                            , username, password);
            this.connected = true;
        } catch (SQLException e) {
            this.console.stacktrace("Can't connect to the database", e);
        }

        return this;
    }

    /**
     * Simplifie la création d'une table dans la base de donnée.
     *
     * @param name      le nom de la table.
     * @param variables les variables de la table.
     * @return un objet {@link fr.triozer.api.database.mysql.Table}
     */
    public Table create(String name, String... variables) {
        StringBuilder vars = new StringBuilder("");

        if (variables.length > 0) {
            vars.append("(").append(variables[0]);
            for (int i = 1; i < variables.length; i++) {
                vars.append(", ").append(variables[i]);
            }
            vars.append(")");
        }

        executeUpdate("CREATE TABLE IF NOT EXISTS `" + name + "` " + vars);
        return new Table(this, name);
    }

    /**
     * Execute an update with a MySQL query.
     *
     * @param query The MySQL query.
     */
    public void executeUpdate(String query) {
        try {
            PreparedStatement sts = this.connection.prepareStatement(query);
            sts.executeUpdate();
            sts.close();
        } catch (SQLException e) {
            if (e.getMessage().contains("Duplicate")) return;
            this.console.stacktrace("Error while " + query, e);
        }
    }

    /**
     * Select an object in the database with a MySQL query.
     *
     * @param query The MySQL query.
     * @param get   The object to get.
     * @return The find object.
     */
    public Object select(String query, String get) {
        Object request = null;

        try {
            PreparedStatement sts    = this.connection.prepareStatement(query);
            ResultSet         result = sts.executeQuery();
            while (result.next())
                request = result.getObject(get);
            sts.close();
        } catch (SQLException e) {
            this.console.stacktrace("Error while " + query + " and get " + get, e);
        }

        return request;
    }

    /**
     * Read a list of values with MySQL query.
     *
     * @param query The MySQL written query.
     * @param get   The field to read.
     * @return Returns found List.
     */
    public List<Object> selectList(String query, String get) {
        List<Object> request = new ArrayList<>();

        try {
            PreparedStatement sts    = this.connection.prepareStatement(query);
            ResultSet         result = sts.executeQuery();
            while (result.next())
                request.add(result.getObject(get));

            sts.close();
        } catch (SQLException e) {
            this.console.stacktrace("Error while " + query, e);
        }

        return request;
    }

    /**
     * Retrieves the string parsed from the list of where values.
     *
     * @param where The list of where values.
     * @return The parsed string.
     */
    public String getListOfWhereValues(List<Field> where) {
        StringBuilder whereFields = new StringBuilder();
        where.forEach(whereField -> whereFields.append(whereField.getParsedNameAndValue()).append(Method.AND));

        return whereFields.toString().substring(0, whereFields.length() - 4);
    }

    /**
     * Close the etablished connection.
     */
    public void close() {
        if (!connected)
            return;

        try {
            this.connection.close();
        } catch (SQLException e) {
            this.console.stacktrace("Error while closing connection.", e);
        }
    }

    /**
     * @return l'objet {@link java.sql.Connection} de la classe.
     */
    public final Connection getConnection() {
        return this.connection;
    }

    /**
     * @return l'adresse d'hôte de la base de donnée.
     */
    public final String getHost() {
        return this.host;
    }

    /**
     * @return l'état de la connexion à la base de donnée.
     */
    public final boolean isConnected() {
        return this.connected;
    }

    /**
     * @return le nom de la base de donnée.
     */
    public final String getDatabase() {
        return this.database;
    }

    /**
     * @return le port de l'hôte de la base de donnée.
     */
    public final int getPort() {
        return this.port;
    }

    /**
     * Retrieves a table into this database with his name.
     *
     * @param name The table name.
     * @return The table object.
     */
    public Table getTable(String name) {
        return new Table(this, name);
    }

}
