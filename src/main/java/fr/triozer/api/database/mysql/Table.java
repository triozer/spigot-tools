package fr.triozer.api.database.mysql;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Cédric / Triozer
 */
public class Table {

    private final MySQL  database;
    private final String name;
    private final String quotesName;

    Table(MySQL database, String name) {
        this.database = database;
        this.name = name;
        this.quotesName = "`" + name + "`";
    }

    public void add(String key) {
        add(key, "VARCHAR(100)");
    }

    public void add(String key, String type) {
        try {
            DatabaseMetaData md = database.getConnection().getMetaData();
            ResultSet        rs = md.getColumns(null, null, name, key);

            if (!rs.next()) {
                database.executeUpdate("ALTER TABLE " + quotesName + " ADD COLUMN `" + key + "` " + type);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Retrieves a value find in the table from a {@link Field}.
     *
     * @param field The field to get.
     * @param where The field to find the row.
     * @return The find value.
     */
    public Object select(String field, Field where) {
        return database.select(Method.SELECT + " `" + field + "` " + Method.FROM +
                quotesName + Method.WHERE + where.getParsedNameAndValue(), field);
    }

    /**
     * Retrieves a value find in the table from
     * a {@link Field} with a list of where values.
     *
     * @param field The field to get.
     * @param where The list of fields to find the row.
     * @return The find value.
     */
    public Object select(String field, List<Field> where) {
        return database.select(Method.SELECT + " `" + field + "` " + Method.FROM +
                quotesName + Method.WHERE + database.getListOfWhereValues(where), field);
    }

    /**
     * Retrieves a list of values find in the table.
     *
     * @param field The field to get.
     * @return The list of values.
     */
    public List<Object> selectList(String field) {
        return database.selectList(Method.SELECT + " `" + field + "` " + Method.FROM + quotesName, field);
    }

    /**
     * Retrieves a list of values find in the table with a
     * specific value in row.
     *
     * @param field The field to get.
     * @param where The field to get the row.
     * @return The list of values find with specific value.
     */
    public List<Object> selectList(String field, Field where) {
        return database.selectList(Method.SELECT + " `" + field + "` " + Method.FROM +
                quotesName + Method.WHERE + where.getParsedNameAndValue(), field);
    }

    /**
     * Retrieves a list of values find in the table with a
     * list of specific values in row.
     *
     * @param field The field to get.
     * @param where The list of fields to get the row.
     * @return The list of values find with specific value.
     */
    public List<Object> selectList(String field, List<Field> where) {
        return database.selectList(Method.SELECT + " `" + field + "` " + Method.FROM +
                quotesName + Method.WHERE + database.getListOfWhereValues(where), field);
    }

    /**
     * Insert a list of field into the table.
     *
     * @param fields The list of fields.
     */
    public void insert(Map fields) {
        //Setup insert fields
        StringBuilder setter = new StringBuilder();
        StringBuilder field  = new StringBuilder();

        fields.forEach((key, value) -> {
            field.append("`").append(key).append("`, ");
            setter.append("'").append(value).append("', ");
        });

        //Update query
        database.executeUpdate(Method.INSERT_INTO + " " + quotesName + "(" +
                field.substring(0, field.length() - 2) + ") " + Method.VALUES +
                "(" + setter.substring(0, setter.length() - 2) + ")");
    }

    public void insert(Field... fields) {
        StringBuilder setter = new StringBuilder();
        StringBuilder field  = new StringBuilder();

        Arrays.stream(fields).forEach((key) -> {
            field.append(key.getParsedName()).append(", ");
            setter.append(key.getParsedValue()).append(", ");
        });

        database.executeUpdate(Method.INSERT_INTO + " " + quotesName + "(" +
                field.substring(0, field.length() - 2) + ") " + Method.VALUES +
                "(" + setter.substring(0, setter.length() - 2) + ")");
    }

    /**
     * Delete a row find by a field.
     *
     * @param where The field to find the row.
     */
    public void delete(Field where) {
        database.executeUpdate(Method.DELETE_FROM + " " + quotesName + Method.WHERE + where.getParsedNameAndValue());
    }

    /**
     * Update a row in the table with the field to set
     * and the field to get the row.
     *
     * @param toSet The field to set.
     * @param where The field to get the row.
     */
    public void update(Field toSet, Field where) {
        database.executeUpdate(Method.UPDATE + quotesName + Method.SET +
                toSet.getParsedNameAndValue() + Method.WHERE + where.getParsedNameAndValue());
    }

    /**
     * Update a row in the table with the field to set
     *
     * @param toSet The field to set.
     */
    public void update(Field toSet) {
        database.executeUpdate(Method.UPDATE + " " + quotesName + " " + Method.SET +
                toSet.getParsedNameAndValue());
    }


    /**
     * Update a row in the table with a list of fields to set
     * and the field to get the row.
     *
     * @param toSet The list of fields to set.
     * @param where The field to get the row.
     */
    public void update(Map<String, Object> toSet, Field where) {
        //Setup updated fields
        StringBuilder setters = new StringBuilder();
        for (Map.Entry<String, Object> entry : toSet.entrySet())
            setters.append("`").append(entry.getKey()).append("` = '").append(entry.getValue()).append("', ");

        //Update query
        database.executeUpdate(Method.UPDATE + quotesName + Method.SET +
                setters.substring(0, setters.length() - 2) + Method.WHERE + where.getParsedNameAndValue());
    }

    public final MySQL getDatabase() {
        return this.database;
    }

    public final String getName() {
        return this.name;
    }

    public final String getQuotesName() {
        return this.quotesName;
    }
}